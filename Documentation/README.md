## How to work on this project : 
* git clone <project url> (the project)
* cd code/challengewebapp (go to the react project folder)
* npm install (to install all the dependencies)
* npm start (to start the project in development mode on port 3000)
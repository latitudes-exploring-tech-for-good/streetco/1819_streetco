const dev = {
  apiGateway: {
    URL: "http://devapi.street-co.com/"
  }
};

const prod = {
  apiGateway: {
    URL: "https://api.street-co.com/"
  }
};


export const config = process.env.REACT_APP_STAGE === 'production'
  ? prod
  : dev;
  

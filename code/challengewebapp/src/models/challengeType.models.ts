export class ChallengeType{

    public static TEAM : number = 1;
    public static TEAM_UNRANKED : number = 2;
    public static SOLO : number = 3;

    public Id : number;
    public Name : string;

}
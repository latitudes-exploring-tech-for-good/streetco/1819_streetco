enum PrezSiteContactFromLocation {
    MainView = 0,
    ApplicationView = 1,
    OfferChallengeView = 2,
    OfferDiagnosticView = 3,
    OfferDiagnosticModal = 4,
    OfferChallengeModal = 5,
    OfferReferencementModal = 6,
    WebAppChallenge = 7
}

export class PrezSiteContactFormModel {
    
    public FromSiteLocation : PrezSiteContactFromLocation;
    public Name : string;
    public Email : string;
    public Message : string;
    public CompanyName : string;
    public IsSubscribeToNewsLetter : boolean;

    constructor(FromSiteLocation : PrezSiteContactFromLocation, Name : string, Email : string, Message : string, CompanyName : string, IsSubscribeToNewsLetter : boolean) {
        this.FromSiteLocation = FromSiteLocation;
        this.Name = Name;
        this.Email = Email;
        this.Message = Message;
        this.CompanyName = CompanyName;
        this.IsSubscribeToNewsLetter = IsSubscribeToNewsLetter;
    }

}
export class User{
    public BirthDate : Date;
    public City : string;
    public LastCityPosition : string;
    public LastCoordsPositionLat : string;
    public LastCoordsPositionLng : string;
    public Email : string;
    public FirstName : string;
    public Id : string;
    public LastConnection : Date;
    public LastConnectionDuringSeconds : number;
    public LastName : string;
    public PushIsEnabled : boolean;
    public AcceptNewCgu : boolean;
    public Type : number;
    public Username : string;
    public EmergencyNumber : string;
    public Roles : string[];
    public IsBlocked : boolean;
    public EmailConfirmed : boolean;
    public Points : number;
    public photoUrl : string;
    public DistanceTraveled : number;
    public Grade : any;
}
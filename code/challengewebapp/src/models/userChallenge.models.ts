export class UserChallenge{
    
    public Id : number;
    public userId : string;
    public TeamChallengeId : number;
    public ChallengeId : number;
    public Points : number;
    public ReportedObstacles : number;
    public ReportedPoi : number;
    public DistanceTraveled : number;
    public FirstName : string;
    public LastName : string;
    public Email : string;
    public Rank : number;
    public TeamRank : number;

}
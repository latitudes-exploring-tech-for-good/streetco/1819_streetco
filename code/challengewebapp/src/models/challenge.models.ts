export class Challenge {

	public Id : number;
	public Name : string;
	public DateStart : string;
	public DateEnd : string;
	public Zone : string;
	public Logo : string;
	public AcceptSolo : boolean;
	public mailEnding : string;
	public ChallengeTypeId : number;

}
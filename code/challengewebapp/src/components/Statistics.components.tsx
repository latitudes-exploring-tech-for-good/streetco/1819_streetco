// src/components/Statistics.components.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';

export class Statistics extends React.Component<any, any>{

    public render() {
        let valueElt;
        if(this.props.challengeStarted){
            valueElt = <div className="statistic-value">{this.props.value}</div>
        }
        return(
            <div className="col-lg-4">
                {valueElt}
                <div className="statistic-description">{this.props.label}</div>
            </div>
        )
        
    }

}
import * as React from 'react';
import { SelectInput } from './FormItems.components';
import arrow from "../assets/img/arrow.png";

export class LanguageSelector extends React.Component<any, any>{

    constructor(props : any){
        super(props);
        this.state = {
            showFlags : false
        }
        this.handleChange = this.handleChange.bind(this);
        this.showFlags = this.showFlags.bind(this);
    }
    
    public render(){
        let selectedIcon;
        const options = this.props.languages.map((language : any) => {
            const className : string = language.shortCut === this.props.currentLanguage? "icon selected" : "icon";
            if (language.shortCut === this.props.currentLanguage){
                selectedIcon = <div className="selected-icon"><img src={language.image} alt={language.shortCut}/></div>
            }
            return (
                <a href="#" onClick={this.handleChange} id={language.shortCut} key={language.shortCut}><div id={language.shortCut} className={className}><img id={language.shortCut} src={language.image} alt={language.shortCut}/></div></a>
            )
        })
        const box = this.state.showFlags? <div className="box">{options}</div> : "";
         return (          
            <div id={this.props.id} className="icon-select">
                <div className="selected-box">
                    <a href="#" onClick={this.showFlags}>
                        {selectedIcon}
                        <div className="component-icon"><img src={arrow} alt="arrow"/></div>
                    </a>
                    {box}
                </div>
            </div>
        ) 
    }

    private handleChange(e : any){
        e.preventDefault();
        this.props.handleChange(e.target.id);
        this.setState({
            showFlags : false
        })
    }

    private showFlags(e : any){
        e.preventDefault();
        this.setState({
            showFlags : !this.state.showFlags
        })
    }

}
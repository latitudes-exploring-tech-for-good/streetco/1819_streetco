import * as React from 'react';
import { texts } from 'src/helpers/texts.helpers';
import { Form, TextInput, TextArea } from 'src/components/FormItems.components';
import {Modal} from 'react-bootstrap';
import logo from '../assets/img/logo.png';
import { PrezSiteContactFormModel } from 'src/models/prezSiteContactFormModel.models';
import { ChallengeService } from 'src/services/challenge.service';
import { InfoBar } from './InfoBar.components';


export default class Grensquare extends React.Component<any,any> {
    
    private challengeService : ChallengeService;

    constructor(props :any){
        super(props);
        this.state = {
          email : "",
          errors : [],
          firstName : "",
          infos : [],
          lastName : "",
          message : "",
          newsLetterSubscribe : false,
          organisationName : "",
          smShow: false,
        };
        this.challengeService = new ChallengeService();
        this.setModal=this.setModal.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleOrganisationNameChange = this.handleOrganisationNameChange.bind(this);
        this.handleMessageChange = this.handleMessageChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNewsLetterSubscriptionChange = this.handleNewsLetterSubscriptionChange.bind(this);
        this.closeInfoBar = this.closeInfoBar.bind(this);
        this.closeErrorBar = this.closeErrorBar.bind(this);
    }

  public render(){
    const infoBar = this.state.infos.length > 0 ? <InfoBar class="alert alert-success" infos={this.state.infos} close={this.closeInfoBar}/> : "";
    const errorBar = this.state.errors.length > 0 ? <InfoBar class="alert alert-danger" infos={this.state.errors} close={this.closeErrorBar}/> : "";
    return (
      <div className="col-lg-3 nopadding row-eq-height">
        <div className="row custom-box right-part">
          <div className="col">
            <p className="text-center">{this.props.text}</p>
            <p className="text-center">
              <button type="button" className="btn btn-light btn-contact" onClick={this.setModal}>{texts.homeTexts.botton}</button> 
              <Modal show={this.state.smShow}>
                <div className="modal-content">
                    <div className="modal-header streetco-gradient">
                      <h5 className="modal-title modal-contact" id="exampleModalLabel"><img src={logo}/></h5>
                        <button type="button" className="close" onClick={this.setModal} aria-label="Close"  >
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <Form method="POST" action="" id="contactForm" validationButton={false}>
                      <div className="modal-body">
                        <div className="row">
                          <h5 className="modal-title-text col-lg-12">{texts.homeTexts.form}</h5>
                          {infoBar}
                          {errorBar}
                          <TextInput className="col-lg-6"  type="text" id="lastname" name="lastname" value={this.state.lastName} placeHolder={texts.homeTexts.lastName} handleChange={this.handleLastNameChange}/>
                          <TextInput className="col-lg-6" type="text"  id="firstname" name="firstname" value={this.state.firstName} placeHolder={texts.homeTexts.firstName} handleChange={this.handleFirstNameChange}/>
                        </div>
                        <div className="row">
                          <TextInput type="email" className="col-lg-6" id="email" name="email" value={this.state.email} placeHolder="Mail" handleChange={this.handleEmailChange}/>
                          <TextInput type="text" className="col-lg-6" id="organisation" name="organisation" value={this.state.organisationName} placeHolder={texts.homeTexts.organisationName} handleChange={this.handleOrganisationNameChange}/>
                        </div>
                        <TextArea rows="5" id="message" name="message" value={this.state.message} placeHolder="Message" handleChange={this.handleMessageChange}/>
                        <div className="row">
                          <div className="col-lg-6 offset-lg-3">
                              <input type="checkbox" id='subscribeNewsLetter' name='subscribeNewsLetter' required={false} onChange={this.handleNewsLetterSubscriptionChange}/> {texts.homeTexts.newsLetterCheckBox}
                          </div>
                        </div>
                      </div>
                      <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={this.setModal}>{texts.homeTexts.closeButton}</button>
                        <button type="submit" className="btn btn-primary" name="sendContact" value="1" onClick={this.handleSubmit}>{texts.homeTexts.sendButton}</button>
                      </div>
                    </Form>
                </div>
              </Modal>
            </p>
          </div>
        </div>
      </div>
    );
  }

  private setModal(){
    this.setState({ smShow: !this.state.smShow})
  }

  private handleFirstNameChange(e : any){
    this.setState({
      firstName : e.target.value
    })
  }

  private handleLastNameChange(e : any){
    this.setState({
      lastName : e.target.value
    })
  }

  private handleEmailChange(e : any){
    this.setState({
      email : e.target.value
    })
  }

  private handleOrganisationNameChange(e : any){
    this.setState({
      organisationName : e.target.value
    })
  }

  private handleMessageChange(e : any){
    this.setState({
      message : e.target.value
    })
  }

  private handleNewsLetterSubscriptionChange(e : any){
    this.setState({
      newsLetterSubscribe : e.target.checked
    })
  }

  private handleSubmit(e : any){
    e.preventDefault();
    const param = new PrezSiteContactFormModel(7, this.state.firstName + " " + this.state.lastName, this.state.email, this.state.message, this.state.organisationName, this.state.newsLetterSubscribe);
    this.challengeService.sendContactForm(param).then((res : any) => {
      if (res === "err"){
        this.setState({
          errors : this.state.errors.concat([texts.homeTexts.contactFormError])
        })
      } else {
        this.setState({
          infos : this.state.infos.concat([texts.homeTexts.contactFormSuccess])
        })
      }
    })
  }

  private closeInfoBar(){
    this.setState({
      infos : []
    })
  }

  private closeErrorBar(){
    this.setState({
      errors : []
    })
  }

}
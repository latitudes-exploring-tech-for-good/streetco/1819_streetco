// src/components/FormItems.components.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { string } from 'prop-types';

export class Form extends React.Component<any, any>{
    
    constructor(props : any){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    public render(){
        const validationButtonElt = this.props.validationButton? <ValidationButton buttonText={this.props.validationButtonText}/> : '';
        return(
            <form className={this.props.formClass} method={this.props.formMethod} onSubmit={this.handleSubmit}>
                {this.props.children}
                {validationButtonElt}
            </form>
        );
    }

    private handleSubmit(e : any){
        this.props.handleSubmit(e);
    }

}

export function ValidationButton(props : any){
    return(
        <button type="submit" className="btn btn-inscription" name={props.buttonName}>{props.buttonText}</button>
    );
}

export function TextInput(props : any){
    return(
        <div className={props.className}>
            <label className="control-label">{props.label}</label>
            <div>
                <input type={props.type} className="form-control" id={props.id} name={props.name} placeholder={props.placeHolder} value={props.value} onChange={props.handleChange} required={true} disabled={props.disabled}/>
            </div>
        </div>
    )
}

export function TextArea (props :any){
  return(
   <div className="row">
   <div className="col-lg-12">
   <textarea className="form-control" rows={props.rows} id={props.id} name={props.name} placeholder={props.placeHolder} onChange={props.handleChange}/>
   </div>
   </div>
)
}

export function SelectInput(props : any){
    return(
        <div className="form-group text-left col-lg-6">
            <label className="control-label">{props.label}</label>
            <select className='form-control' disabled={props.disabled} id={props.id} name={props.name} defaultValue={props.defaultValue} onChange={props.handleChange}>
                {props.options}
            </select>
            <label className="control-label">{props.label2}</label>
        </div>
    )
}
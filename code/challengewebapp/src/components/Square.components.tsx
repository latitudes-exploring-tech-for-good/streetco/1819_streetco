import * as React from 'react';

export  default class Square extends React.Component<any, any> {
    
    constructor(props : any){
        super(props);
        }
    

    public render(){

    return (
        <div className="col-lg-3">
          <p className="text-center"><img src={this.props.img}/></p>
          <p className="text-center">{this.props.text}</p>
        </div>
    );
    }
}
// src/components/RankingArray.components.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { text } from '@fortawesome/fontawesome-svg-core';
import { texts } from 'src/helpers/texts.helpers';
import { UserChallenge } from 'src/models/userChallenge.models';
import { ChallengeService } from 'src/services/challenge.service';
import { ChallengeTeam } from 'src/models/challengeTeam.models';
import Loading from 'src/views/client/Loading.client.views';

export class RankingArray extends React.Component<any,any>{

    private challengeService : ChallengeService;

    constructor(props : any){
        super(props);
        this.state = {
            challengeTeams : null,
            hasChallengeEnded : false,
            hasChallengeStarted : true,
            page : 1,
            userChallenges : null
        }
        this.challengeService = new ChallengeService();
        this.handleUserClick = this.handleUserClick.bind(this);
        this.setParameters = this.setParameters.bind(this);
        this.setPage = this.setPage.bind(this);
    }

    public componentDidMount(){
        this.setParameters();
    }

    public render(){
        const col3Header = this.props.challenge? texts.rankingTexts.col3_1 : texts.rankingTexts.col3_2;
        let rows : any;
        if (this.props.challenge){ // a challenge exists
            if (this.props.isAccessRestricted){
                rows = (
                    <tr>
                        <td colSpan={4}>
                            {texts.rankingTexts.restrictedAccess}
                        </td>
                    </tr>
                )
            }
            else if (this.state.hasChallengeEnded){
                rows = (
                    <tr>
                        <td colSpan={4}>
                            {texts.rankingTexts.endedChallenge}
                        </td>
                    </tr>
                )
            } else if (!this.state.hasChallengeStarted) {
                rows = (
                    <tr>
                        <td colSpan={4}>
                            {texts.rankingTexts.notStartedChallenge}
                        </td>
                    </tr>
                )
            } else if (this.state.userChallenges){ // the data has been fetched from the API 
                rows = this.state.userChallenges.map((userChallenge : UserChallenge) => {
                    const imgElt = userChallenge.Rank <= 3? <img src={require('../assets/img/trophee'+String(userChallenge.Rank)+'.png')} alt='trophee'/> : "";
                    let teamName : string = "-";
                    this.state.challengeTeams.forEach((challengeTeam : ChallengeTeam) => {
                        if (challengeTeam.Id === userChallenge.TeamChallengeId){
                            teamName = challengeTeam.Name;
                        }
                    });
                    if (this.props.filter.replace(/ /g, "") === "" || teamName.toUpperCase().includes(this.props.filter.toUpperCase()) || userChallenge.LastName.toUpperCase().includes(this.props.filter.toUpperCase()) || userChallenge.FirstName.toUpperCase().includes(this.props.filter.toUpperCase())){
                        return (
                            <tr key={userChallenge.Id} id={String(userChallenge.Id)} onClick={this.handleUserClick}>
                                <td>{imgElt} {userChallenge.Rank}</td>
                                <td>{userChallenge.LastName.toUpperCase()} {userChallenge.FirstName}</td>
                                <td>{teamName}</td>
                                <td>{userChallenge.Points}</td>
                            </tr>
                        );
                    } else {
                        return
                    }
                });
                rows = rows.filter((row : any) => {
                    return row
                });
                const maxResultsPage : number = 100;
                const nbPages : number = Math.ceil(rows.length/maxResultsPage);
                const pages = [];
                for (let i=1; i<=nbPages; i++){
                    this.state.page===i? pages.push(<li className='list-inline-item hoverNumber' id="blackNumber" onClick={this.setPage} key={i}>{i}</li>) : pages.push(<li className='list-inline-item hoverNumber' onClick={this.setPage} key={i}>{i}</li>);
                }
                rows = rows.slice(maxResultsPage*(this.state.page-1),maxResultsPage*this.state.page)
                rows.push(
                <tr key={"pages"}>
                    <td colSpan={4} className='bottom'>
                        <nav className='navbar navbar-default ranking-pages'>
                            <ul className='list-inline'><li className='list-inline-item' key={0}>Page :</li>{pages}</ul>
                        </nav>
                    </td>
                </tr>)
            } else {
                rows = (
                    <tr>
                        <td colSpan={4}>
                            <Loading/>
                        </td>
                    </tr>
                ) 
            }
            
            
        } else { // there is no existing challenge (happens only if a user who takes part in no challenge opens the ranking tab)
            rows = (
                <tr>
                    <td colSpan={4}>
                        {texts.rankingTexts.noChallenge}
                    </td>
                </tr>
            )
        }
        return(
            <div id="layout-content" className="layout-content-wrapper container">
                <table  className="table" data-toggle="table" data-toolbar="#toolbar" data-search="false" data-cache="false" id="ranking">
                    <thead className="fixedHeader">
                        <tr>
                            <th scope="col">{texts.rankingTexts.col1Header}</th>
                            <th scope="col">{texts.rankingTexts.col2Header}</th>
                            <th scope="col">{col3Header}</th>
                            <th scope="col">{texts.rankingTexts.col4Header}</th>
                        </tr>
                    </thead>
                    <tbody className="scrollContent">
                        {rows}
                    </tbody>
                </table>
            </div>
        )  
    }

    private handleUserClick(e : any){
        this.challengeService.getUserChallengeFromUserChallengeId(Number(e.target.parentNode.id), this.props.challenge.Id).then((userChallenge : UserChallenge) => {
            this.props.setDashboard(userChallenge)
        })
    }

    private setParameters(){
        if (this.props.challenge){
            return this.challengeService.getUserChallengesInChallenge(this.props.challenge.Id).then((userChallenges : UserChallenge[]) => {
                return this.challengeService.getChallengeTeamsFromChallengeId(this.props.challenge.Id).then((challengeTeams : ChallengeTeam[]) => {
                    this.setState({
                        challengeTeams,
                        hasChallengeEnded : this.challengeService.hasChallengeEnded(this.props.challenge),
                        hasChallengeStarted : this.challengeService.hasChallengeStarted(this.props.challenge),
                        userChallenges
                    });
                })
            })
        } else {
            return
        }
    }

    private setPage(e : any){
        this.setState({
            page : Number(e.target.childNodes[0].data)
        });
    }

}

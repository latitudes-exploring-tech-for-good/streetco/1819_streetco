// src/components/ChallengesArray.components.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Challenge } from '../models/challenge.models';
import { ChallengeService } from '../services/challenge.service';
import { texts } from 'src/helpers/texts.helpers';

export default class ChallengesArray extends React.Component<any,any> { 
  public challengeService:ChallengeService;
  
  constructor(props: any) {
    super(props);
	  this.challengeService = new ChallengeService();
    this.state = {challengesAndParticipantsNb: []};
    this.handleChallengeClick = this.handleChallengeClick.bind(this);
  } 

  public async componentDidMount() { 
    const challengesAndParticipantsNb : any[] = [];
    const challenges = await this.challengeService.getChallenges();
    challenges.forEach((challenge : Challenge) => {
      this.challengeService.getChallengerNumberFromChallengeId(challenge.Id).then((challengersNb : number) => {
        challengesAndParticipantsNb.push({challenge, challengersNb});
        this.setState({ challengesAndParticipantsNb });
      })
    })
    
  }

  public render() { 
    const dateParameter = this.props.language === "fr"? "fr-FR" : "en-GB";
    const challenges = this.state.challengesAndParticipantsNb.map((challengeAndParticipantsNb : any, i:any) => (
          <tr key={i} id={String(challengeAndParticipantsNb.challenge.Id)} onClick={this.handleChallengeClick}>
            <td>{ challengeAndParticipantsNb.challenge.Name }</td>
            <td>{texts.homeTexts.from} { new Date(challengeAndParticipantsNb.challenge.DateStart).toLocaleString(dateParameter)} {texts.homeTexts.to} { new Date(challengeAndParticipantsNb.challenge.DateEnd).toLocaleString(dateParameter) }</td>
            <td>{challengeAndParticipantsNb.challengersNb}</td>
          </tr>    
    ));
    return (
      <div id="layout-content" className="custom-box challenges-table">
        <table className="table" data-toggle="table" data-toolbar="#toolbar" data-search="true" id="challenges">
          <thead className="fixedHeader">
            <tr>
              <th scope="col">{texts.homeTexts.challengesCol1} </th>
              <th scope="col">{texts.homeTexts.challengesCol2}</th>
              <th scope="col">{texts.homeTexts.challengesCol3}</th>
            </tr>
          </thead>
          <tbody className="scrollContent">
            { challenges }
          </tbody>
        </table> 
      </div>
    );
  }
  private handleChallengeClick(e : any){
    this.challengeService.getChallengeFromChallengeId(Number(e.target.parentNode.id)).then((challenge : Challenge) => {
      this.props.handleChallengeClick(challenge);
    })
  }
}

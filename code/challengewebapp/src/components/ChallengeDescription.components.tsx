import * as React from 'react';

export default class ChallengeDescription extends React.Component<any,any> {
    
    constructor(props :any){
        super(props);
        }
    

    public render(){

    return (
         <div className="col-lg-12 intro bg-w-semi-trans">
		<p>{this.props.p1}</p>
		<p>{this.props.p2}</p>
	  </div>

    );
    }
}
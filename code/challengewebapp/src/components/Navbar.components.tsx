// src/components/Navbar.components.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import logo_blue from '../assets/img/logo_blue.png' // relative path to image 
import { texts } from 'src/helpers/texts.helpers';
import { LanguageSelector } from './LanguageSelector.components';

export default class Navbar extends React.Component<any,any> { 

	constructor(props : any){
		super(props);
		this.state = {
			tabs : this.props.isConnected ? texts.commonTexts.connectedTabs : texts.commonTexts.notConnectedTabs
		}
		this.handleTabClick = this.handleTabClick.bind(this);
		this.handleLogoClick = this.handleLogoClick.bind(this);
	}

	public componentWillReceiveProps(nextProps: { isConnected: any; }){
		this.setState({
			tabs : this.props.isConnected ? texts.commonTexts.connectedTabs : texts.commonTexts.notConnectedTabs
		});
	}

	public render(){
		const currentTab = this.props.tab;
		const tabList = this.state.tabs.map((tab : any) => {
			return <Tab value={tab.name} tabNumber={tab.number} onClick={this.handleTabClick} key={tab.number} active={currentTab === tab.number}/>
		});
		const tabListMobile = this.state.tabs.map((tab : any) => {
			const activeClass = currentTab === tab.number? "active" : "";
			return(
				<div className="nav-element cut-text" key={tab.number}>
						<a href='#' onClick={this.handleTabClick} id={String(tab.number)} className={activeClass}>{tab.name}</a>
				</div>   
			)
		});
		return (
			<div>
				<nav className="navbar navbar-expand-lg navbar-dark fixed-top custom-shadow">
					<div className="container">
						<div className="spaceBetweenFlex">
							<a className="navbar-brand" href="#" onClick={this.handleLogoClick}><img src={logo_blue} alt='logo'/></a>
							<LanguageSelector id="smallScreen" handleChange={this.props.changeLanguage} value={this.props.language} languages={this.props.languages} currentLanguage={this.props.language}/>
						</div>
						<div className="collapse navbar-collapse" id="navbarResponsive">
								<ul className="navbar-nav ml-auto">{tabList}</ul>
						</div>
						<div className="navbarMobile" id="navbarResponsiveMobile">
							{tabListMobile}
						</div>
					</div>
					<LanguageSelector id="bigScreen" handleChange={this.props.changeLanguage} value={this.props.language} languages={this.props.languages} currentLanguage={this.props.language}/>
				</nav>
			</div>
		);
	}	

	private handleTabClick(e:any){
		e.preventDefault();
		this.props.handleClick(Number(e.target.id));
	}

	private handleLogoClick(e : any){
			e.preventDefault();
			this.props.logoClick();
	}

}

function Tab(props : any){
	if (props.active){
			return(
					<li className="nav-item">
							<a className="nav-link active" href='#' onClick = {props.onClick} id={props.tabNumber}>{props.value}</a>
					</li>
			)
	} else {
			return(
					<li className="nav-item">
							<a className="nav-link" href='#' onClick = {props.onClick} id={props.tabNumber}>{props.value}</a>
					</li>
			)
	}
}

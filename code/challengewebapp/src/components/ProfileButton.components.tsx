// src/components/ProfileButton.components.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export function ProfileButton(props:any){
    return (
        <button className={props.buttonClass} onClick={props.handleClick} disabled={props.disabled}><b>{props.buttonText} <FontAwesomeIcon icon={props.icon} className="icon"/></b></button>
    )
}
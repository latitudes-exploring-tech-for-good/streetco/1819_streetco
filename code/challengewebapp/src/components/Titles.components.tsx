import * as React from 'react';

export default class Titles extends React.Component<any,any> {
    
    constructor(props : any){
        super(props);
        }
    

    public render(){
        return (
            <div>
                <h1 className="mt-5">{this.props.title}</h1>
                <p className="lead">{this.props.subtitle}</p>
            </div>
        );
    }
}
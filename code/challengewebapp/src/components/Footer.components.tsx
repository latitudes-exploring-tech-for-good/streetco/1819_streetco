// src/components/Footer.components.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import logo_blue from '../assets/img/logo_blue.png'; // relative path to image 
import logo_white from '../assets/img/logo_white.png';
import { texts } from 'src/helpers/texts.helpers';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faApple, faGooglePlay, faFacebook, faTwitter, faYoutube, faInstagram, faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

library.add(faApple, faGooglePlay, faFacebook, faTwitter, faYoutube, faInstagram, faLinkedin);

export function Footer(props : any){
    const prezSiteLink : string = "https://" + props.language + ".street-co.com/";
    let link1;
    let link2;
    let link3;
    let link4;
    let link5;
    let link6;
    let link7;
    let link8;
    let link9;
    switch (props.language){
        case "fr" : {
            link1 = "faq";
            link2 = "contact";
            link3 = "mentions-legales";
            link4 = "cgu";
            link5 = "politique-de-confidentialite";
            link6 = "equipe";
            link7 = "https://blog.street-co.com/";
            link8 = "offres";
            link9 = "recrutement";
          break;
        }
        case "en" : {
          link1 = "faq";
          link2 = "contact";
          link3 = "legal-notice";
          link4 = "terms-of-service";
          link5 = "privacy-policy";
          link6 = "team";
          link7 = "https://blog.street-co.com/";
          link8 = "offers";
          link9 = "job-offers";
          break;
        }
    }
    return(
        <footer className="custom-shadow">
            <div className='container-fluid'>
                <div className='row'>
                    <div className="col-lg-4 col-ms-12 align-self-end">
                        <div className="footer-left">
                            <img alt="logo Streetco" src={logo_white}/>
                            <p>
                                {texts.commonTexts.footerStreetcoDescription}
                            </p>
                            <div className="download-actions ng-star-inserted">
                                <a target="_blank" href="https://itunes.apple.com/fr/app/streetco/id1110776262">
                                    <button className="btn download footer-download" type="button">
                                        <span className="icon"><FontAwesomeIcon icon={faApple} className="icon"/></span>
                                        <span className="text">
                                            <span className="min"> {texts.commonTexts.availableOn}</span>
                                            <span className="max"> {texts.commonTexts.appStore}</span>
                                        </span>
                                    </button>
                                </a>
                                <a target="_blank" href="https://play.google.com/store/apps/details?id=com.pmrstreet.gps">
                                    <button className="btn download footer-download" type="button">
                                        <span className="icon"><FontAwesomeIcon icon={faGooglePlay} className="icon"/></span>
                                        <span className="text">
                                            <span className="min"> {texts.commonTexts.availableOn}</span>
                                            <span className="max"> {texts.commonTexts.googlePlay}</span>
                                        </span>
                                    </button>
                                </a>
                            </div>
                            <ul className="social">
                                <li className="ng-star-inserted">
                                    <a target="_blank" href="https://www.facebook.com/StreetcoApp/"><FontAwesomeIcon icon={faFacebook} className="icon"/></a>
                                </li>
                                <li className="ng-star-inserted">
                                    <a target="_blank" href="https://twitter.com/streetcoapp?lang=en"><FontAwesomeIcon icon={faTwitter} className="icon"/></a>
                                </li>
                                <li className="ng-star-inserted">
                                    <a target="_blank" href="https://www.instagram.com/streetcoapp/?hl=en"><FontAwesomeIcon icon={faInstagram} className="icon"/></a>
                                </li>
                                <li className="ng-star-inserted">
                                    <a target="_blank" href="https://www.youtube.com/channel/UCzfNkntiFnipua1m1Q-EHlA"><FontAwesomeIcon icon={faYoutube} className="icon"/></a>
                                </li>
                                <li className="ng-star-inserted">
                                    <a target="_blank" href="https://www.linkedin.com/company/pmr-street/?originalSubdomain=en"><FontAwesomeIcon icon={faLinkedin} className="icon"/></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-8 col-md-12 links">
                        <div className="row">
                            <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 hide_lg"/>
                            <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 hide_lg">
                                <h4>{texts.commonTexts.footerColumn1Title}</h4>
                                <ul>
                                    <li><a target="_blank" href={prezSiteLink + link1}>{texts.commonTexts.footerColumn11}</a></li>
                                    <li><a target="_blank" href={prezSiteLink + link2}>{texts.commonTexts.footerColumn12}</a></li>
                                    <li><a target="_blank" href={prezSiteLink + link3}>{texts.commonTexts.footerColumn13}</a></li>
                                    <li><a target="_blank" href={prezSiteLink + link4}>{texts.commonTexts.footerColumn14}</a></li>
                                    <li><a target="_blank" href={prezSiteLink + link5}>{texts.commonTexts.footerColumn15}</a></li>
                                </ul>
                            </div>
                            <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 hide_lg">
                            <h4>{texts.commonTexts.footerColumn2Title}</h4>
                                <ul>
                                    <li><a target="_blank" href={prezSiteLink + link6}>{texts.commonTexts.footerColumn21}</a></li>
                                    <li><a target="_blank" href={link7}>{texts.commonTexts.footerColumn22}</a></li>
                                    <li><a target="_blank" href={prezSiteLink + link8}>{texts.commonTexts.footerColumn23}</a></li>
                                    <li><a target="_blank" href={prezSiteLink + link9}>{texts.commonTexts.footerColumn24}</a></li>
                                </ul>
                            </div>
                            <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 hide_lg"/>
                        </div>
                    </div>  
                </div>
            </div>
        </footer>
    )
}
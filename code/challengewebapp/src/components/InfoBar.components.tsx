import * as React from 'react';

export class InfoBar extends React.Component<any, any>{

    public render(){
        let key : number = 0;
        const infos = this.props.infos.map((info : any) => {
            key += 1;
            return <li key={key}>{info}</li>
        });
        return (
            <div className={this.props.class} id={this.props.id}>
                    <span id="close" onClick={this.props.close}>x</span>
                    <br/>
                    <ul>
                        {infos}
                    </ul>
            </div>
        )
    }

}
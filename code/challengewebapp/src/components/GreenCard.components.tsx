// src/components/GreenCard.components.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';

export class GreenCard extends React.Component<any, any>{

    constructor(props : any){
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    public render() {
        const clickable = this.props.clickable? " clickable" : "";
        return (
            <div className="col-lg-5 col-md-8 offset-md-2 text-center">
                <div className={"green-card"+clickable} onClick={this.handleClick}>
                    <div className="row">
                        <div className="col-lg-2 image">
                            <img src={this.props.img} />
                        </div>
                        <div className="col-lg-3 value">
                            <span>{this.props.value}</span><span className="total-rank">{this.props.value2}</span>
                        </div>
                        <div className="col-lg-3 description">
                            <span>{this.props.label}</span>
                        </div>
                    </div>
                </div>
            </div>
        )
        
    }

    private handleClick(){
        this.props.handleClick(3);
    }

}
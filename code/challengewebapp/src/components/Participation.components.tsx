import * as React from 'react';

export class Participation extends React.Component<any, any> {
    
    constructor(props : any){
        super(props);
        }
    

    public render(){
        return (
            <div className="col-lg-4 custom-box">
                <label>{this.props.number}</label>
                    <p>{this.props.text}</p>
                    <div className='row'>
                    {this.props.children}
                    </div>
            </div>
        );
    }


}

export function Store(props : any){
    return (
    <div className='col-lg-6'>
        <a href={props.href}>
            <img className={props.class} alt='Disponible sur Google Play' src={props.src}/>
        </a>
    </div>
    );
}

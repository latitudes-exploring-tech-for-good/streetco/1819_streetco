import { FrTexts } from './texts.fr.helpers';
import { EnTexts } from './texts.en.helpers';
import france from '../assets/img/france.png';
import unitedKingdom from '../assets/img/united-kingdom.png';

export const languages : any[] = [{shortCut : "fr", image : france, texts : FrTexts}, {shortCut : "en", image : unitedKingdom, texts : EnTexts}];

export let texts = FrTexts;

export function changeLanguage(lang : string){
    languages.forEach((langObj) => {
        if (langObj.shortCut === lang){
            texts = langObj.texts;
        }
    })
}
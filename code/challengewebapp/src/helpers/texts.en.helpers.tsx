export class EnTexts{
    
    public static homeTexts : any = {
        
        at : "at",
        botton: "Contact us",
        challengesCol1: "Challenge",
        challengesCol2: "Date",
        challengesCol3: "Participants",
        closeButton : "Close",
        contactFormError : "An error occured while sending the form. Please try again later.",
        contactFormSuccess : "Your message has been successfully sent ! We will answer you very soon !",
        description1 : "The Streetco challenge's goal is to raise awareness of accessibility and disability issues in a fun way. Participants are mobilized to carry out concrete action of general interest while creating links on the company.",
        description2 : "All challengers are encouraged to use the Streetco application to report obstacles (work areas, impassable sidewalks) and accessible places they encounter during their daily commutes. The more participants report, the more they improve accessibility around them and the more points they earn to get up in the ranking.",
        firstName : "First Name",
        form: "Contact us",
        from : "From",
        lastName : "Last Name",
        newsLetterCheckBox : "I wish to subscribe to Streetco's Newsletter",
        organisationName : "Name of your organisation or your city",
        sendButton : "Send",
        square1 : "Create links between participants",
        square2: "Raise awareness in a fun, innovative and extremely concrete way",
        square3 : "Put employees at the heart of the citizen’s approach",
        square4: "Increase the value of your organization's or city's image internally and externally",
        squaregreen: "You want to organize a challenge in your company, your organization or in your city ?",
        subtitle : "Take part in the Streetco Challenge !",
        subtitle3 : "Join your organization's/your city's challenge!",
        text1: "First, download the free Streetco app on your personal or professional mobile phone.",
        text2: "On the application click directly on “Join a challenge”, then select your challenge (and team if available)",
        text3: "Enter your information ( name, first name and  business email address preferred ). Caution, your points earned on the Streetco app will be counted in your challenge only once the challenge has started.",
        title1 : "Your Streetco Challenge",
        title2: "How to participate ?",
        title3: "Challenges Space",
        to : "to"
        
    }
    
    public static commonTexts : any = {
        appStore : "app store",
        availableOn : "available on",
        connectedTabs : [{name : "Home", number : 0}, {name : "Ranking", number : 3}, {name : "Profile", number : 4}],
        footerColumn11 : "FAQ",
        footerColumn12 : "Contact Us",
        footerColumn13 : "Legal Notice",
        footerColumn14 : "TOS",
        footerColumn15 : "Privacy Policy",
        footerColumn1Title : "SUPPORT",
        footerColumn21 : "Who are we?",
        footerColumn22 : "Streetco Blog",
        footerColumn23 : "Our offers",
        footerColumn24 : "Join Streetco Team",
        footerColumn2Title : "ABOUT US",
        footerStreetcoDescription : "Streetco is the first collaborative pedestrian GPS application dedicated to people with reduced mobility.",
        googlePlay : "google play",
        notConnectedTabs : [{name : "Home", number : 0}, {name : "Register", number : 1}, {name : "Login", number : 2}]
    }
    
    public static loginTexts : any = {
        emailLabel : "Your Email on Streetco",
        emailPlaceholder : "Streetco email",
        loginFail : 'Wrong Email or Password',
        passwordLabel : "Your password on Streetco",
        passwordPlaceholder : "Streetco password",
        submitButtonText : "Submit",
        title : "Log in"
    }
    
    public static profileTexts : any = {
        connectionConfirmationButNoChallengeMessage : "Your Streetco credentials are valid but you aren't subscribed to any challenge yet",
        connectionConfirmationMessage : "You have been successfully logged in !",
        emailLabel : "Streetco mail",
        emailPlaceholder : "Streetco mail",
        firstNameError : "The first name isn't correct",
        firstNameLabel : "First name",
        firstNamePlaceholder : "First name",
        individualTeamOption1 : "Solo",
        lastNameError : "The last name isn't correct",
        lastNameLabel : "Last name",
        lastNamePlaceholder : "Last name",
        logoutButton : "Log out",
        progressButton : "See your progress",
        submitButtonText : "Submit",
        subscribeButton : "Subscribe",
        teamLabel : "Your Team",
        title : "Your Streetco Challenge profile",
        updateNamesError : "An error occured while editing the information. Please try again later.",
        updateNamesMessage : "Your profile has been successfully updated !"
    }
    
    public static dashboardTexts : any = {
        SOLOchallengeTypeName : "Solo challenge",
        TEAM_UNRANKEDchallengeTypeName : "Challenges by team and individual ranking",
        TEAMchallengeTypeName : "Challenges by team",
        greenLabel1 : "POINTS",
        greenLabel2 : "CHALLENGE RANKING",
        greenLabel3 : "RANKING WITHIN THE TEAM",
        greenLabel4 : "TEAM'S RANKING",
        podium1 : "Streetco",
        podium2 : "Podium !",
        stat1 : "Reported obstacles",
        stat2 : "Reported accessible locations",
        stat3 : "Kilometers travelled",
        subTitle1 : "Ranking : ",
        subTitle2 : "The challenge will start on the",
        teamLabel : "Team",
        title1 : "Dashboard of ",
        title2 : "Statistics"
    }

    public static rankingTexts : any = {
        challengeNotBegun : "Your organization's challenge hasn't begun yet, please be patient before starting the competition.",
        col1Header : "Rank",
        col2Header : "Participant",
        col3_1 : "Team",
        col3_2 : "Company",
        col4Header : "Points",
        endedChallenge : "This challenge is over",
        general : "General",
        modalButton1 : "Back to Home",
        modalButton2 : "Submit",
        modalError : "This Email isn't allowed for this challenge",
        modalLabel : "Please enter your professional email to access the ranking.",
        noChallenge : "You don't belong in any challenge",
        notStartedChallenge : "No results found",
        of : "of",
        requiredEmail : "Required Email",
        restrictedAccess : "Restricted Access",
        searchPlaceholderBeggining : "Search",
        searchPlaceholderEnd1 : "Team, Participant",
        searchPlaceholderEnd2 : "Organization, Participant",
        subtitle : "Ranking ",
        title : "Streetco Challenge"
    }

    public static subscriptionTexts : any = {
        alertMessage : "In order to count your points and distinguish yourself from the other streeters Streeters, merci de bien renseigner les mêmes informations (email, nom, prénom) que sur l'application Streetco.",
        challengeLabel : "Challenge",
        challengePlaceholder : "Select a Challenge",
        challengersNumber : "Number of challengers in this team : ",
        checkBox1 : "By subscribing to the Streetco challenge, I agree with the",
        checkBox2 : "General Conditions of Use and Privacy Policy",
        checkBox3 : "of the Streetco application",
        connexionWithDataBaseError : "Connexion problem with the database. ¨lease try again later.",
        emailError : "This email adress doesn't exist in our database or is already registered in Streetco Challenge. Please check your email adress, or download the application on the Appstore or on Google Play in order to join us !",
        emailLabel : "Streetco mail",
        emailPlaceholder : "",
        firstNameError : "The first name isn't correct",
        lastNameError : "The last name isn't correct",
        loginError : "Wrong Password. Please try again",
        nameLabel : "First Name",
        namePlaceholder : "",
        passwordLabel : "Your password on Streetco",
        passwordPlaceholder : "Streetco password",
        registerError : "An error occured while saving the information. Please try again later.",
        registerSuccess : "Registration successfully completed.",
        soloTeamName : "Solo",
        submitButtonText : "Submit",
        subtitle : "Take part in a Streetco Challenge by signing up via this form !",
        surnameLabel : "Last Name",
        surnamePlaceholder : "",
        teamLabel : "Team\n(Once the challenge has started you will not be able to change it)",
        teamLabel2 : "Number of challengers in this team : ",
        teamPlaceholder : "Select a Team",   
        title : "Your Streetco Challenge",
    }

}
    

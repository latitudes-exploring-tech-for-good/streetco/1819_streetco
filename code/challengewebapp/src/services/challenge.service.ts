
import { ChallengeApiService } from './api/challenge.api.service';
import { Challenge } from 'src/models/challenge.models';
import { ChallengeTeam } from 'src/models/challengeTeam.models';
import { UserChallenge } from 'src/models/userChallenge.models';
import { ChallengeType } from 'src/models/challengeType.models';
import { PrezSiteContactFormModel } from 'src/models/prezSiteContactFormModel.models';

export class ChallengeService {
    public challengeApiService:ChallengeApiService;
	
    constructor() { 
	this.challengeApiService = new ChallengeApiService();
	}
	
	public getChallenges(getOnlyNotEnded:boolean = true) {
        return this.challengeApiService.getChallenges(getOnlyNotEnded).then((challenges) => {
			return challenges;
		},
		(rejected) => {
			throw rejected;
		});
	}

	public getChallengeFromChallengeId(challengeId : number) : any {
		return this.challengeApiService.getChallenges(false).then((challenges) => {
			let rightChallenge;
			challenges.forEach((challenge) => {
				if (challenge.Id === challengeId){
					rightChallenge = challenge;
				}
			});
			if (rightChallenge){
				return rightChallenge as Challenge;
			} else {
				return null;
			}
		},
		(rejected) => {
			throw rejected;
		});
	}

	public getChallengeTeamFromChallengeTeamId(teamId : number, challengeId : number){
		return this.challengeApiService.getChallengeTeamsFromChallengeId(challengeId).then((challengeTeams) => {
			let rightChallengeTeam;
			challengeTeams.forEach((challengeTeam) => {
				if (challengeTeam.Id === teamId){
					rightChallengeTeam = challengeTeam;
				}
			});
			if (rightChallengeTeam){
				return rightChallengeTeam as ChallengeTeam;
			} else {
				return null;
			}
		},
		(rejected) => {
			throw rejected;
		})
	}

	public getChallengerNumberFromChallengeId(challengeId : number) : Promise<number>{
		return this.challengeApiService.getChallengerNumberFromChallengeId(challengeId).then((challengerNumber) => {
			return challengerNumber as number;
		},
		(rejected) => {
			throw rejected;
		})
	}

	public getChallengeTeamsFromChallengeId(challengeId : number) : Promise<ChallengeTeam[]>{
		return this.challengeApiService.getChallengeTeamsFromChallengeId(challengeId).then((challengeTeams) => {
			return challengeTeams as ChallengeTeam[];
		},
		(rejected) => {
			throw rejected;
		})
	}

	public getUserChallengeInfos(userId : string) {
		return this.challengeApiService.getUserChallengeInfos(userId).then((userChallenge) => {
			return userChallenge as UserChallenge;
		},
		(rejected) => {
			throw rejected;
		})
	}

	public getUserChallengesInChallenge(challengeId : number){
		return this.challengeApiService.getUserChallengesInChallenge(challengeId).then((userChallenges : UserChallenge[]) => {
			return userChallenges as UserChallenge[];
		},
		(rejected) => {
			throw rejected;
		})
	}

	public hasChallengeStarted(challenge : Challenge) : boolean {
		const now = new Date();
        const yearNow = now.getFullYear();
        const monthNow = now.getMonth() + 1; // getMonth() returns number between 0 and 11...
        const dayNow = now.getDate();
        const hoursNow = now.getHours();
        const minutesNow = now.getMinutes();
		const secondsNow = now.getSeconds();
        if (yearNow > Number(challenge.DateStart.slice(0,4))){
            return true;
        } else if (yearNow === Number(challenge.DateStart.slice(0,4))) {
            if (monthNow > Number(challenge.DateStart.slice(5,7))){
                return true;
            } else if (monthNow === Number(challenge.DateStart.slice(5,7))){
                if (dayNow > Number(challenge.DateStart.slice(8,10))){
                    return true;
                } else if (dayNow === Number(challenge.DateStart.slice(8,10))){
                    if (hoursNow > Number(challenge.DateStart.slice(11,13))){
                        return true;
                    } else if (hoursNow === Number(challenge.DateStart.slice(11,13))){
                        if (minutesNow > Number(challenge.DateStart.slice(14,16))){
                            return true;
                        } else if (minutesNow === Number(challenge.DateStart.slice(14,16))) {
                            if (secondsNow > Number(challenge.DateStart.slice(17,19))) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
	}
	
	public hasChallengeEnded(challenge : Challenge) : boolean {
		const now = new Date();
        const yearNow = now.getFullYear();
        const monthNow = now.getMonth() + 1;
        const dayNow = now.getDate();
        const hoursNow = now.getHours();
        const minutesNow = now.getMinutes();
		const secondsNow = now.getSeconds();
        if (yearNow > Number(challenge.DateEnd.slice(0,4))){
            return true;
        } else if (yearNow === Number(challenge.DateEnd.slice(0,4))) {
            if (monthNow > Number(challenge.DateEnd.slice(5,7))){
                return true;
            } else if (monthNow === Number(challenge.DateEnd.slice(5,7))){
                if (dayNow > Number(challenge.DateEnd.slice(8,10))){
                    return true;
                } else if (dayNow === Number(challenge.DateEnd.slice(8,10))){
                    if (hoursNow > Number(challenge.DateEnd.slice(11,13))){
                        return true;
                    } else if (hoursNow === Number(challenge.DateEnd.slice(11,13))){
                        if (minutesNow > Number(challenge.DateEnd.slice(14,16))){
                            return true;
                        } else if (minutesNow === Number(challenge.DateEnd.slice(14,16))) {
                            if (secondsNow > Number(challenge.DateEnd.slice(17,19))) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
	}

	public getUserChallengeFromUserChallengeId(userChallengeId : number, challengeId : number){
		return this.getUserChallengesInChallenge(challengeId).then((userChallenges : UserChallenge[]) => {
			let rightUserChallenge : UserChallenge = userChallenges[0];
			userChallenges.forEach((userChallenge : UserChallenge) => {
				if (userChallenge.Id === userChallengeId){
					rightUserChallenge = userChallenge;
				}
			})
			return rightUserChallenge;
		})
    }
    
    public checkEmailForChallenge(email : string, challengeId : number){
        return this.challengeApiService.checkEmailForChallenge(email, challengeId).then((res : boolean) => {
            return res as boolean;
        },
        (rejected) => {
			throw rejected;
		})
	}
	
	public checkEmailForRegistration(email : string){
		try {
			return this.challengeApiService.checkEmailForRegistration(email).then((res : boolean) => {
				return res as boolean;
			}).catch((err) => {
				return "err";
			})
		}
        catch (rejected){
			throw rejected;
		}
	}

	public registerUser(userId : string, email : string, challengeId : string, challengeTeamId : string, firstName : string, lastName : string) {
		try {
			return this.challengeApiService.registerUser(userId, email, challengeId, challengeTeamId, firstName, lastName).then((res :any) => {
				return res as any;
			}).catch((err) => {
				return "err";
			})
		}
        catch (rejected) {
			throw rejected;
		}
	}

	public sendContactForm(prezSiteContactForm : PrezSiteContactFormModel) {
		try {
			return this.challengeApiService.sendContactForm(prezSiteContactForm).then((res :any) => {
				return res as any;
			}).catch((err) => {
				return "err";
			})
		}
        catch (rejected) {
			throw rejected;
		}
	}

}
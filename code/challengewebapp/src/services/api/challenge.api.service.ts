import * as Config from '../../config';
import { Challenge } from '../../models/challenge.models';
import axios from 'axios';
import { resolve } from 'url';
import { ChallengeTeam } from 'src/models/challengeTeam.models';
import { UserChallenge } from 'src/models/userChallenge.models';
import { PrezSiteContactFormModel } from 'src/models/prezSiteContactFormModel.models';

export class ChallengeApiService {
	
	public getChallenges = (getOnlyNotEnded:boolean = true, $top?:number, $skip?:number,$orderBy?:string) : Promise<Challenge[]>  => {
		
		let url = Config.config.apiGateway.URL + `api/challenge/getchallenges?getOnlyNotEnded=`+getOnlyNotEnded;
		url+= "&0=0"+($top?"&$top="+$top :"") + ($skip?"&$skip="+$skip :"") + ($orderBy?"&$orderBy="+$orderBy :"");
				
		return axios.get(url)
			  .then(res => {
				return Promise.resolve( res.data.results as Challenge[]);
			  }).catch(err => {
				return Promise.reject(Error(err.response))
			  });
	};

	public getChallengeTeamsFromChallengeId(challengeId : number, $top?:number, $skip?:number,$orderBy?:string) : Promise<ChallengeTeam[]> {
		let url = Config.config.apiGateway.URL + 'api/challenge/getteams?challengeId=' + challengeId;
		url += "&0=0"+($top?"&$top="+$top :"") + ($skip?"&$skip="+$skip :"") + ($orderBy?"&$orderBy="+$orderBy :"");
		return axios.get(url)
		.then(res => {
			return Promise.resolve( res.data.results as ChallengeTeam[]);
		}).catch(err => {
			return Promise.reject(Error(err.response))
		});
	}

	public getChallengerNumberFromChallengeId(challengeId : number) : Promise<number>{
		const url = Config.config.apiGateway.URL + 'api/challenge/getuserschallengecount?challengeId='+challengeId;
		return axios.get(url)
		.then(res => {
			return Promise.resolve(res.data);
		}).catch(err => {
			return Promise.reject(Error(err.response));
		})
	}

	public getUserChallengeInfos(userId : string){
		const url = Config.config.apiGateway.URL + 'api/challenge/getuserchallengebyuserid?userId=' + userId;
		const headers = {
			"Content-Type": "application/x-www-form-urlencoded"
		};
		return axios.get(url)
		.then(res => {
			return Promise.resolve(res.data);
		}).catch(err => {
			return Promise.reject(Error(err.response));
		})
	}

	public getUserChallengesInChallenge(challengeId : number, $search ?: string, $top ?: number, $skip ?: number){
		let url = Config.config.apiGateway.URL + 'api/challenge/getuserschallenge?challengeId=' + challengeId;
		url += '&0=0' + ($search?"&$search=" + $search : '') + ($top?"&$top="+$top :"") + ($skip?"&$skip="+$skip :"");
		return axios.get(url)
		.then(res => {
			return Promise.resolve( res.data.results as UserChallenge[]);
		}).catch(err => {
			return Promise.reject(Error(err.response))
		});
	}

	public checkEmailForChallenge(email : string, challengeId : number){
		const url = Config.config.apiGateway.URL + 'api/challenge/isvalidemail?email=' + email + '&challengeId=' + challengeId;
		return axios.get(url)
		.then(res => {
			return Promise.resolve(res.data);
		}).catch(err => {
			return Promise.reject(Error(err.response));
		})
	}

	public checkEmailForRegistration(email : string){
		const url = Config.config.apiGateway.URL + 'api/challenge/isemailregisteredwithoutchallenge?email=' + email;
		return axios.get(url)
		.then(res => {
			return Promise.resolve(res.data);
		}).catch(err => {
			return Promise.reject(Error(err.response));
		})
	}

	public registerUser(userId : string, email : string, challengeId : string, challengeTeamId : string, firstName : string, lastName : string) {
		const url = Config.config.apiGateway.URL + 'api/challenge/registeruserwithexistingemail';
		const params = new URLSearchParams();
		params.append("userId", userId);
		params.append("email", email);
		params.append("challengeId" , challengeId);
		params.append("teamChallengeId" , challengeTeamId);
		params.append("firstName", firstName);
		params.append("lastName", lastName);
		return axios.post(url, params)
		.then(() => {return}).catch(err => {
			return Promise.reject(Error(err.response));
		})
	}

	public sendContactForm(prezSiteContactForm : PrezSiteContactFormModel) {
		const url = Config.config.apiGateway.URL + 'api/prezsite/sendcontactform';
		const params = new URLSearchParams();
		// params.append("PrezSiteContactFromLocation", JSON.stringify(prezSiteContactForm));
		params.append("FromSiteLocation", String(prezSiteContactForm.FromSiteLocation));
		params.append("Name", prezSiteContactForm.Name);
		params.append("Email", prezSiteContactForm.Email);
		params.append("Message", prezSiteContactForm.Message);
		params.append("CompanyName", prezSiteContactForm.CompanyName);
		params.append("IsSubscribeToNewsLetter", String(prezSiteContactForm.IsSubscribeToNewsLetter)); 
		return axios.post(url, params)
		.then((res) => {console.log(res); return res}).catch(err => {
			return Promise.reject(Error(err.response));
		})
	}

}
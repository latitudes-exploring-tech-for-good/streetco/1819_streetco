import * as Config from '../../config';
import axios from 'axios';
import { string } from 'prop-types';
import { UserChallenge } from 'src/models/userChallenge.models';

export class AuthApiService {
	
	public getToken = (email : string, password : string) : Promise<any>  => {
		const url = Config.config.apiGateway.URL + 'token';
		const params = new URLSearchParams();
		params.append("client_id", "MOBILE");
		params.append("client_secret", "NotSoSecret");
		params.append("grant_type", "password");
		params.append("password", password);
		params.append("username", email);
		return axios.post(url, params).then(res => {
			return Promise.resolve(res.data as any);
		}).catch(err => {
			return Promise.reject('Wrong email or password');
		});
	}

	public getUserInfos(tokenObject : any, updateConnectionDate : boolean = true) : Promise<any> {
		const url = Config.config.apiGateway.URL + 'api/account/info?updateConnectionDate=' + updateConnectionDate;
		const headers = {
			"Authorization" : "Bearer " + tokenObject.access_token,
			"Content-Type": "application/x-www-form-urlencoded"
		}
		return axios.get(url, {
			headers
		}).then(res => {
			return Promise.resolve(res.data);
		}).catch(err => {
			return Promise.reject(Error(err.response));
		});
	}

	public logoff(tokenObject : any) : Promise<any> {
		const url = Config.config.apiGateway.URL + 'api/account/logoff';
		const headers = {
			"Authorization" : "Bearer " + tokenObject.access_token,
			"Content-Type": "application/x-www-form-urlencoded"
		}
		return axios.post(
			url, 
			{headers}
		).then(() => {return}).catch(err => {
			return Promise.reject(Error(err.response))
		})
	}

	public updateNames(tokenObject : any, firstName : string, lastName : string){
		const url = Config.config.apiGateway.URL + 'api/challenge/updateuserinformation';
		const headers = {
			"Authorization" : "Bearer " + tokenObject.access_token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
		const params = new URLSearchParams();
		params.append("FirstName", firstName);
		params.append("LastName", lastName);
		return axios.post(url, params, {
			headers
		}).then(res => {
			return Promise.resolve(res as any);
		}).catch(err => {
			return Promise.reject("Failure in the names' update")
		})
	}

}
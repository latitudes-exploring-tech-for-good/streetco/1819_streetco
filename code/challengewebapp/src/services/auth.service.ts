
import { AuthApiService } from './api/auth.api.service';
import { UserChallenge } from 'src/models/userChallenge.models';
import { User } from 'src/models/user.models';

export class AuthService {
    public authApiService : AuthApiService;
	
    constructor() { 
		this.authApiService = new AuthApiService();
	}
	
	public login(email : string, password : string) : Promise<any> {
    try {
			return this.authApiService.getToken(email, password).then((res) => {
				return this.authApiService.getUserInfos(res).then((user) => {
					return {tokenObject : res, user : user as User};
				})
			}).catch((err) => {
				return false;
			})
		}
		catch (rejected) {
			throw rejected;
		}
	}

	public logout(tokenObject : any){
		try {
			return this.authApiService.logoff(tokenObject);
		}
		catch (rejected) {
			throw rejected;
		}
	}

	public updateNames(tokenObject : any, firstName : string, lastName : string) : Promise<any>{
		try {
			return this.authApiService.updateNames(tokenObject, firstName, lastName)
			.catch((err) => {
				return false
			});
		}
		catch (rejected) {
			throw rejected;
		}
	}

	public getConnectedUser(tokenObject : any) : Promise<User>{
		try {
			return this.authApiService.getUserInfos(tokenObject);
		}
		catch (rejected) {
			throw rejected;
		}
	}

}
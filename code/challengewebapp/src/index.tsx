import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Home from './views/client/Home.client.views';
import Subscription from './views/client/Subscription.client.views';
import Login from './views/client/Login.client.views';
import StreetcoRanking from './views/client/StreetcoRanking.client.views';
import Profile from './views/client/Profile.client.views';
import 'bootstrap/dist/css/bootstrap.css';
import './assets/style.css';
import registerServiceWorker from './registerServiceWorker';
import Navbar from './components/Navbar.components';
import { Footer } from './components/Footer.components';
import { UserChallenge } from './models/userChallenge.models';
import Dashboard from './views/client/Dashboard.client.views';
import { ChallengeService } from './services/challenge.service';
import Loading from './views/client/Loading.client.views';
import { User } from './models/user.models';
import { Challenge } from './models/challenge.models';
import { AuthService } from './services/auth.service';
import { languages } from './helpers/texts.helpers';
import { changeLanguage } from './helpers/texts.helpers';
import { ProfileButton } from './components/ProfileButton.components';

class App extends React.Component<any, any>{
  
  private challengeService : ChallengeService;
  private authService : AuthService;

  constructor(props : any){
    super(props);
    this.state = {
      connectedUser : null, // the user who is currently connected or null
      isLoading : true, // when isLoading is true, the loading view is displayed, else the view corresponding to tab number is displayed
      language : "fr",
      profileErrors : [],
      profileInfos : [], // infos to display in the profile view (we need it here because of the AppUpdate when a user changes his name)
      selectedChallenge : null, // if not null, then the Ranking displayed in the ranking tab is the one of this challenge. If null, it will be the one of the connected user's current challenge if he has one
      selectedUserChallenge : null, // the user we will pass as props for the dashboard view (not always the connected user because a user can view other users' dashboards)
      tab : 0, // corresponds to the view that has to be displayed
      tokenObject : null // if a user is connected, this object contains all the tokens and stuff
    };
    this.challengeService = new ChallengeService();
    this.authService = new AuthService();
    this.setTab = this.setTab.bind(this);
    this.logoClick = this.logoClick.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.setDashboard = this.setDashboard.bind(this);
    this.isLoading = this.isLoading.bind(this);
    this.updateUserAndInfos = this.updateUserAndInfos.bind(this);
    this.setRanking = this.setRanking.bind(this);
    this.clearProfileInfos = this.clearProfileInfos.bind(this);
    this.clearProfileErrors = this.clearProfileErrors.bind(this);
    this.handleSubscribeButtonClick = this.handleSubscribeButtonClick.bind(this);
    this.changeLanguage = this.changeLanguage.bind(this);
  }

  public componentDidMount() {
    let tokenObject = sessionStorage.getItem('connexionTokenObject');
    const tab = sessionStorage.getItem('tab')? Number(sessionStorage.getItem('tab')) : 0;
    const language : string = sessionStorage.getItem('language')? String(sessionStorage.getItem('language')) : "fr";
    if (language !== "fr"){
      this.changeLanguage(language);
    }
    if (tokenObject){
      tokenObject = JSON.parse(tokenObject);
      this.authService.getConnectedUser(tokenObject).then((user : User) => {
        this.challengeService.getUserChallengeInfos(user.Id).then((userChallenge : UserChallenge) => {
          if (userChallenge){
            this.challengeService.getChallengeFromChallengeId(userChallenge.ChallengeId).then((challenge : Challenge) => { // we need to initialize the selected Challenge with the connected user's current challenge
              this.setState({
                connectedUser : user,
                language,
                selectedChallenge : challenge,
                tab,
                tokenObject
              }, () => {
                this.isLoading();
              });
            })
          } else {
            this.setState({
              connectedUser : user,
              language,
              tab,
              tokenObject
            }, () => {
              this.isLoading();
            })
          }
          
        })
      })
    } else {
      this.setState({
        language,
        tab
      }, () => {
        this.isLoading();
      })
    }
  }

  public render() {
    let view;
    if(this.state.isLoading){
      view = <Loading/>
    } else {
      switch (this.state.tab){
        case 0 : {
          view = <Home handleChallengeClick={this.setRanking} language={this.state.language}/>;
          break;
        }
        case 1 : {
          view = <Subscription language={this.state.language} handleRegistration={this.handleLogin}/>;
          break;
        } 
        case 2 : {
          view = <Login handleLogin={this.handleLogin} isLoading={this.isLoading} handleSubscribeButtonClick={this.handleSubscribeButtonClick}/>;
          break;
        }
        case 3 : {
          view = <StreetcoRanking user={this.state.connectedUser} challenge={this.state.selectedChallenge} setDashboard={this.setDashboard} setTab={this.setTab}/>;
          break;
        }
        case 4 : {
          view = <Profile tokenObject={this.state.tokenObject} user={this.state.connectedUser} handleProgressButtonClick={this.setDashboard} handleLogoutButtonClick={this.handleLogout} isLoading={this.isLoading} updateAppState={this.updateUserAndInfos} profileInfos={this.state.profileInfos} profileErrors={this.state.profileErrors} clearProfileInfos={this.clearProfileInfos} clearProfileErrors={this.clearProfileErrors}/>;
          break;
        }
        case 5 : {
          view = <Dashboard userChallenge={this.state.selectedUserChallenge} handleChallengeClick={this.setRanking}/>;
          break;
        }
        default : {
          view = <Loading/>
          break;
        }
      };
    }
    return (
      <div className="whole-content">
        <Navbar isConnected={Boolean(this.state.connectedUser)} tab={this.state.tab} handleClick={this.setTab} logoClick={this.logoClick} language={this.state.language} changeLanguage={this.changeLanguage} languages={languages}/>
        <div id="home-content" className="container view">
          {view}
        </div>
        <div className="footer-content">
          <Footer language={this.state.language}/>
        </div>
      </div>
          
    );
  }

  private setTab(value : number){
    sessionStorage.setItem('tab', String(value));
    this.setState({tab : value});
  }

  private setDashboard(userChallenge : UserChallenge){
    this.setState({
      selectedUserChallenge : userChallenge,
      tab : 5
    })
  }

  private handleLogin(tokenObj : any, user : User, messages1 : any[], messages2 : any[]){
    sessionStorage.setItem('connexionTokenObject', JSON.stringify(tokenObj));
    sessionStorage.setItem('tab', '4');
    this.clearProfileErrors();
    this.clearProfileInfos();
    this.challengeService.getUserChallengeInfos(user.Id).then((userChallenge : UserChallenge) => {
      if (userChallenge && userChallenge.ChallengeId){
        this.challengeService.getChallengeFromChallengeId(userChallenge.ChallengeId).then((challenge : Challenge) => {
          this.setState({
            connectedUser : user,
            profileInfos : this.state.profileInfos.concat(messages1),
            selectedChallenge : challenge,
            tab : 4,
            tokenObject : tokenObj
          }, () => {
            this.setState({
              isLoading : false
            })
          });
        })
      } 
      else {
        this.setState({
          connectedUser : user,
          profileErrors : this.state.profileErrors.concat(messages2),
          tab : 4,
          tokenObject : tokenObj
        }, () => {
          this.setState({
            isLoading : false
          })
        });
      }
    })
  }

  private logoClick(){
    sessionStorage.setItem('tab', '0');
    this.setState({tab : 0});
  }

  private handleLogout(){
    sessionStorage.clear();
    sessionStorage.setItem("language" , this.state.language);
    sessionStorage.setItem("tab" , "2");
    this.setState({
      connectedUser : null,
      profileErrors : [],
      profileInfos : [],
      selectedChallenge : null,
      selectedUserChallenge : null,
      tab : 2,
      tokenObject : null
    });
  }

  private isLoading(){
    this.setState({
      isLoading : !this.state.isLoading
    });
  }

  private updateUserAndInfos(newInfos : string[], newErrors : string[]){
    this.authService.getConnectedUser(this.state.tokenObject).then((user: User) => {
      this.setState({
        connectedUser : user,
        profileErrors : newErrors,
        profileInfos : newInfos
      })
    })
  }

  private setRanking(challenge : Challenge){
    this.setState({
      selectedChallenge : challenge,
      tab : 3
    })
  }

  private clearProfileInfos(){
    this.setState({
      profileInfos : []
    })
  }

  private clearProfileErrors(){
    this.setState({
      profileErrors : []
    })
  }

  private handleSubscribeButtonClick(){
    sessionStorage.setItem('tab', '1');
    this.setState({
      profileInfos : [],
      tab : 1
    })
  }

  private changeLanguage(lang : string){
    changeLanguage(lang);
    sessionStorage.setItem("language" , lang);
      this.setState({
        language : lang
      })
  }

}

ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();

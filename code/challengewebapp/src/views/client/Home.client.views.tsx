// src/views/client/Home.client.views.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Titles from '../../components/Titles.components';
import ChallengeDescription from '../../components/ChallengeDescription.components';
import  {Store, Participation} from '../../components/Participation.components';
import Greensquare from '../../components/Greensquare.components';
import Square from '../../components/Square.components';
import people from '../../assets/img/people.png';
import light from '../../assets/img/light.png';
import target from '../../assets/img/target.png';
import star from '../../assets/img/star.png';
import { texts } from 'src/helpers/texts.helpers';
import ChallengesArray from '../../components/ChallengesArray.components';

// The Home view corresponds to Tab number 0 
export default class Home extends React.Component<any,any> { 

	constructor(props : any){
		super(props);
	}
	public render() {
		return (
			<div>
				<div className="row">
					<div className="col-lg-12 text-center">
						<Titles title={texts.homeTexts.title1} subtitle={texts.homeTexts.subtitle}/>
					</div>
				</div>
				<ChallengeDescription p1={texts.homeTexts.description1} p2={texts.homeTexts.description2}/>
				<div className="row nomargin">
					<div className="col-lg-9 nopadding row-eq-height">
						<div className="row col-lg-12 custom-box left-part">
							<Square text={texts.homeTexts.square1} img={people}/>
							<Square text={texts.homeTexts.square2} img={light}/>
							<Square text={texts.homeTexts.square3} img={target}/>
							<Square text={texts.homeTexts.square4} img={star}/>
						</div>
					</div>
					<Greensquare text={texts.homeTexts.squaregreen} />
				</div>	
				<div className="row">
					<div className="col-lg-12 text-center">
						<Titles title={texts.homeTexts.title2}/>
					</div>
				</div>
				<div className="col-lg-12 row howtosubscribe">
					<Participation number= {1} text={texts.homeTexts.text1} >
						<Store className='play' href='https://play.google.com/store/apps/details?id=com.pmrstreet.gps&hl=fr&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' src='https://play.google.com/intl/en_us/badges/images/generic/fr_badge_web_generic.png'/>
						<Store href='https://itunes.apple.com/fr/app/streetco/id1110776262?mt=8' src='https://linkmaker.itunes.apple.com/fr-fr/badge-lrg.svg?releaseDate=2017-04-18&kind=iossoftware&bubble=ios_apps'/>
					</Participation>
					<Participation number= {2} text= {texts.homeTexts.text2} />
					<Participation number= {3} text= {texts.homeTexts.text3} />
				</div>
				<div className="row">
					<div className="col-lg-12 text-center">
						<Titles title={texts.homeTexts.title3} subtitle={texts.homeTexts.subtitle3}/>
					</div>
				</div>
				<ChallengesArray handleChallengeClick={this.props.handleChallengeClick} language={this.props.language}/>
			</div>
		);
	}

}
// src/views/client/Login.client.views.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Titles from 'src/components/Titles.components';
import {Form, TextInput } from 'src/components/FormItems.components';
import {AuthService} from '../../services/auth.service';
import { texts } from 'src/helpers/texts.helpers';
import { InfoBar } from 'src/components/InfoBar.components';
import Loading from './Loading.client.views';
import { ProfileButton } from 'src/components/ProfileButton.components';


// The Login view corresponds to Tab number 2
export default class Login extends React.Component<any,any> { 
    
    private authService:AuthService;

	constructor(props : any){
        super(props);
        this.state = {
            email : '',
            error : false,
            isLoading : false,
            password : ''
        }
        this.authService = new AuthService();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.closeErrorBar = this.closeErrorBar.bind(this);
        this.isLoading = this.isLoading.bind(this);
	}

    public render() {
        if (!this.state.isLoading){
            const errorBar = this.state.error? <InfoBar close={this.closeErrorBar} infos={[texts.loginTexts.loginFail]} class="alert alert-danger"/> : "";
            return (
                <div className="row bg-w-semi-trans padding-15">
                    <div className="col-lg-12 text-center">
                        <Titles title={texts.loginTexts.title}/>
                        {errorBar}
                        <Form formClass='login-form' formMethod='POST' validationButton={true} validationButtonText={texts.loginTexts.submitButtonText} buttonName="login" handleSubmit={this.handleSubmit}>
                            <div className="row">
                                <TextInput label={texts.loginTexts.emailLabel} type="email" id="email" name="email" placeHolder={texts.loginTexts.emailPlaceholder} handleChange={this.handleEmailChange} className="form-group text-left col-lg-6"/>
                                <TextInput label={texts.loginTexts.passwordLabel} type="password" id="password" name="password" placeHolder={texts.loginTexts.passwordPlaceholder} handleChange={this.handlePasswordChange} className="form-group text-left col-lg-6"/>
                            </div>
                        </Form>
                    </div>
                </div>
            );
        } else {
            return <Loading/>
        } 
    }

    private handleEmailChange(e : any){
        this.setState({
            email : e.target.value
        })
    }

    private handlePasswordChange(e : any){
        this.setState({
            password : e.target.value
        })
    }

    private async handleSubmit(e : any){
        e.preventDefault();
        this.isLoading();
        const res = await this.authService.login(this.state.email, this.state.password);
        if (res){
            this.props.handleLogin(res.tokenObject, res.user, [texts.profileTexts.connectionConfirmationMessage], [texts.profileTexts.connectionConfirmationButNoChallengeMessage, <ProfileButton key="subscribeButton" buttonClass="btn btn-primary" buttonText={texts.profileTexts.subscribeButton} handleClick={this.props.handleSubscribeButtonClick} disabled={false}/>]);
        } else {
            this.setState({
                error : true
            }, () => {
                this.isLoading();
            })
        }
    }

    private closeErrorBar(){
        this.setState({
            error : false
        })
    }

    private isLoading(){
        this.setState({
            isLoading : !this.state.isLoading
        })
    }

}
// src/views/client/Dashboard.client.views.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Titles from 'src/components/Titles.components';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { Challenge } from 'src/models/challenge.models';
import { ChallengeType } from 'src/models/challengeType.models';
import { ChallengeTeam } from 'src/models/challengeTeam.models';
import { ChallengeService } from 'src/services/challenge.service';
import { texts } from 'src/helpers/texts.helpers';
import Loading from './Loading.client.views';
import { UserChallenge } from 'src/models/userChallenge.models';
import { Statistics } from 'src/components/Statistics.components';
import { GreenCard } from 'src/components/GreenCard.components';
import chart from '../../assets/img/chart.png';
import arrowstar from '../../assets/img/arrowstar.png';

library.add(faArrowLeft);

// The Dashboard view corresponds to Tab number 5
export default class Dashboard extends React.Component<any,any>{

    private challengeService : ChallengeService;

    constructor(props : any){
        super(props);
        this.state = {
            challenge : null,
            challengeParticipantsNb : 0,
            challengeTeam : null,
            challengeTeamNb : 0,
            peopleInChallengeTeamNb : 0,
        }
        this.challengeService = new ChallengeService();
        this.handleArrowClick = this.handleArrowClick.bind(this);
        this.setUserChallengeAndParameters = this.setUserChallengeAndParameters.bind(this);
        this.getChallengeTypeName = this.getChallengeTypeName.bind(this);
        this.doNothing = this.doNothing.bind(this);
    }

    public componentDidMount(){
        this.setUserChallengeAndParameters();
    }

    public render() {
        if (this.state.challenge){
            const hasChallengeStarted = this.challengeService.hasChallengeStarted(this.state.challenge);
            const startingDateElement = !hasChallengeStarted ? (
            <div className="col-lg-8 offset-lg-2">
                <h3>{texts.dashboardTexts.subTitle2} {this.state.challenge.DateStart}</h3>
            </div>
            ) : '';
            const teamName : string = this.state.challengeTeam? this.state.challengeTeam.Name : "-";
            const points : number = hasChallengeStarted? this.props.userChallenge.Points : 0;
            const rank : number = hasChallengeStarted? this.props.userChallenge.Rank : "-";
            const rankingTitle : string = this.getChallengeTypeName();
            const optionalGreenCard = ChallengeType.TEAM === this.state.challenge.ChallengeTypeId? <GreenCard handleClick={this.handleArrowClick} challengeStarted={hasChallengeStarted} userChallenge={this.props.userChallenge} label={texts.dashboardTexts.greenLabel4} img={arrowstar} value={this.props.userChallenge.TeamRank} value2={'/' + this.state.challengeTeamNb} clickable={true}/> : '';
            const optionalGreenCard2 = ChallengeType.SOLO === this.state.challenge.ChallengeTypeId? <GreenCard handleClick={this.doNothing} challengeStarted={hasChallengeStarted} userChallenge={this.props.userChallenge} label={texts.dashboardTexts.greenLabel3} img={arrowstar} value={this.props.userChallenge.Rank} value2={"/" + this.state.peopleInChallengeTeamNb}/> : "";
            const podiumImgSrc : string = "trophee" + String(this.props.userChallenge.Rank);
            const podiumElt = this.props.userChallenge.Rank < 4 && this.props.userChallenge.Rank > 0 && hasChallengeStarted ? <div className="col-lg-4 text-center leader">
                <div className="col-lg-12"><img className="user-rank" src={require('../../assets/img/'+podiumImgSrc+'.png')} /> </div>
                <div className="col-lg-12 info">{texts.dashboardTexts.podium1}<br/>{texts.dashboardTexts.podium2}</div>
            </div> : '';
            return (
                <div>
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <Titles title = {texts.dashboardTexts.title1+ " " + this.props.userChallenge.FirstName + " " + this.props.userChallenge.LastName}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 bg-w-semi-trans">
                            <div className="row">
                                <div className="col-lg-12 custom-box custom-action bg-trans" onClick={this.handleArrowClick}>
                                    <FontAwesomeIcon icon="arrow-left" className="icon"/>
                                    <img className="custom-shadow" src={this.state.challenge.Logo}/>
                                    <div className="identity-arrow-top"/>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4"/>
                        <div className="col-lg-4"/>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <h3 className="lead mt-6">{texts.dashboardTexts.subTitle1 + rankingTitle}</h3>
                        </div>
                    </div>
                    <div className="custom-box user-infos bg-w-semi-trans">
                        {startingDateElement}
                        <div className="stats-disposition">
                            <div className="col-lg-5 col-md-8 offset-md-2 text-center user-info">
                                <p className="user-name">{this.props.userChallenge.LastName} {this.props.userChallenge.FirstName}</p>
                                <p className="user-team">{texts.dashboardTexts.teamLabel} : {teamName}</p>
                            </div>
                            <GreenCard handleClick={this.doNothing} challengeStarted={hasChallengeStarted} img={chart} value={points} label={texts.dashboardTexts.greenLabel1} clickable={false}/>
                        </div>
                        <div className="stats-disposition">
                            {optionalGreenCard}
                            {optionalGreenCard2}
                            <GreenCard handleClick={this.handleArrowClick} challengeStarted={hasChallengeStarted} img={arrowstar} value={rank} label={texts.dashboardTexts.greenLabel2} clickable={true} value2={"/"+this.state.challengeParticipantsNb}/>
                        </div>
                        <div className="stats-disposition">
                            {podiumElt}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <Titles title={texts.dashboardTexts.title2}/>
                        </div>
                    </div>
                    <div className="custom-box user-statistics text-center bg-w-semi-trans">
                        <div className="row">
                            <Statistics challengeStarted={hasChallengeStarted} label={texts.dashboardTexts.stat1} value={this.props.userChallenge.ReportedObstacles}/>
                            <Statistics challengeStarted={hasChallengeStarted} label={texts.dashboardTexts.stat2} value={this.props.userChallenge.ReportedPoi}/>
                            <Statistics challengeStarted={hasChallengeStarted} label={texts.dashboardTexts.stat3} value={this.props.userChallenge.DistanceTraveled}/>
                        </div>
                    </div>
                </div>
            );
        } else {
            return <Loading/>
        }
    }

    private handleArrowClick() : void {
        this.props.handleChallengeClick(this.state.challenge);
    }

    private setUserChallengeAndParameters(){
        return this.challengeService.getChallengeFromChallengeId(this.props.userChallenge.ChallengeId).then((challenge : Challenge) => {
            return this.challengeService.getChallengeTeamFromChallengeTeamId(this.props.userChallenge.TeamChallengeId, this.props.userChallenge.ChallengeId).then((challengeTeam : ChallengeTeam) => {
                return this.challengeService.getChallengeTeamsFromChallengeId(this.props.userChallenge.ChallengeId).then((challengeTeams : ChallengeTeam[]) => {
                    return this.challengeService.getUserChallengesInChallenge(challenge.Id).then((userChallenges : UserChallenge[]) => {
                        let count : number = 0;
                        if (challengeTeam){
                            userChallenges.forEach((userChallengeI : UserChallenge) => {
                                if (userChallengeI.TeamChallengeId === challengeTeam.Id){
                                    count += 1;
                                }
                            });
                        } else {
                            userChallenges.forEach((userChallengeI : UserChallenge) => {
                                if (!userChallengeI.TeamChallengeId || userChallengeI.TeamChallengeId === 0){
                                    count += 1;
                                }
                            });
                        }
                        this.setState({
                            challenge,
                            challengeParticipantsNb : userChallenges.length,
                            challengeTeam,
                            challengeTeamNb : challengeTeams.length,
                            peopleInChallengeTeamNb : count,
                        })
                    })
                })
            })
        })
    }

    private getChallengeTypeName() : string {
        switch (this.state.challenge.ChallengeTypeId) {
            case 1 : {
                return texts.dashboardTexts.TEAMchallengeTypeName;
            }
            case 2 : {
                return texts.dashboardTexts.TEAM_UNRANKEDchallengeTypeName;
            }
            case 3 : {
                return texts.dashboardTexts.SOLOchallengeTypeName
            }
            default : {
                return '';
            }
        }
    }

    private doNothing() : boolean {
        return true;
    }

}
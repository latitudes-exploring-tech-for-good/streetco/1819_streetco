// src/views/client/StreetcoRanking.client.views.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Titles from 'src/components/Titles.components';
import { texts } from 'src/helpers/texts.helpers';
import { ChallengeService } from 'src/services/challenge.service';
import { Challenge } from 'src/models/challenge.models';
import Loading from './Loading.client.views';
import { TextInput } from 'src/components/FormItems.components';
import { RankingArray } from 'src/components/RankingArray.components';
import { InfoBar } from 'src/components/InfoBar.components';
import { Modal } from 'react-bootstrap';

// The StreetcoRanking view corresponds to Tab number 3
export default class StreetcoRanking extends React.Component<any,any> { 
	
    private challengeService : ChallengeService;

    constructor(props : any){
        super(props);
        this.state = {
            isLoading : true,
            modalEmail : "",
            searchBar : "",
            showInfoBar : false,
            showModal : true,
            showModalError : false,
        }
        this.challengeService = new ChallengeService();
        this.closeInfoBar = this.closeInfoBar.bind(this);
        this.setParameters = this.setParameters.bind(this);
        this.handleSearchChange = this.handleSearchChange.bind(this);
        this.handleModalEmailChange = this.handleModalEmailChange.bind(this);
        this.handleHomeReturn = this.handleHomeReturn.bind(this);
        this.checkEmail = this.checkEmail.bind(this);
    }
    
    public componentDidMount(){
        this.setParameters();
    }

  public render() {
    if (!this.state.isLoading){
        const subtitleEnd = this.props.challenge? ' ' + texts.rankingTexts.of + ' ' + this.props.challenge.Name : texts.rankingTexts.general
        const infoBar = this.state.showInfoBar? <InfoBar close={this.closeInfoBar} infos={[texts.rankingTexts.challengeNotBegun]} class="alert alert-info" id="info_challenge_not_started"/> : '';
        const searchPlaceholderEnd : string = this.props.challenge? texts.rankingTexts.searchPlaceholderEnd1 : texts.rankingTexts.searchPlaceholderEnd2;
        const modalError = this.state.showModalError? <div id="error_auth" className="col-lg-8 offset-lg-2">{texts.rankingTexts.modalError}</div> : "";
        return (
            <div>
                <div className="row">
                    <div className="col-lg-12 text-center page-rank-content">
                        <Titles title = {texts.rankingTexts.title} subtitle = {texts.rankingTexts.subtitle + subtitleEnd}/>
                        <div className="custom-box challenges-table bg-w-semi-trans">
                            {infoBar}
                            <div className="fixed-table-toolbar">
                                <TextInput label="" type="text" id="" name="" value={this.state.searchBar} placeHolder={texts.rankingTexts.searchPlaceholderBeggining + " " + searchPlaceholderEnd} handleChange={this.handleSearchChange} disabled={false} className="float-right search col-lg-3 side-padding-0"/>
                            </div>
                            <RankingArray challenge={this.props.challenge} setDashboard={this.props.setDashboard} filter={this.state.searchBar} isAccessRestricted={this.state.showModal}/>
                        </div>
                    </div>
                </div>
                <Modal show={this.state.showModal} backdrop="static" keyboard={false}>
                    <div className="modal-content">
                        <div className="modal-header streetco-gradient">
                            <h4 className="modal-title">{texts.rankingTexts.requiredEmail}</h4>
                        </div>
                        <div className="modal-body text-center">
                            <TextInput label={texts.rankingTexts.modalLabel} type="email" id="email_authorize" placeHolder="dupont@entreprise.com" handleChange={this.handleModalEmailChange} disabled={false} value={this.state.modalEmail}/>
                            {modalError}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" onClick={this.handleHomeReturn}>{texts.rankingTexts.modalButton1}</button>
                            <button type="button" className="btn btn-primary" id="authorize_request" onClick={this.checkEmail}>{texts.rankingTexts.modalButton2}</button>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    } else {
        return <Loading/>
    }
    
    }

    private closeInfoBar(){
        this.setState({
            showInfoBar : false
        })
    }

    private setParameters(){
        let showModal : boolean;
        let hasChallengeStarted : boolean;
        if (this.props.challenge){
            showModal = !(sessionStorage.getItem("NotShowModalForChallenge"+String(this.props.challenge.Id)) === "1");
            hasChallengeStarted = this.challengeService.hasChallengeStarted(this.props.challenge);
        } else {
            showModal = false;
            hasChallengeStarted = true;
        }
        this.setState({
            isLoading : false,
            showInfoBar : !hasChallengeStarted,
            showModal
        })
    }

    private handleSearchChange(e : any){
        this.setState({
            searchBar : e.target.value
        })
    }

    private handleModalEmailChange(e : any){
        this.setState({
            modalEmail : e.target.value
        })
    }

    private handleHomeReturn(){
        this.props.setTab(0);
    }

    private checkEmail(){
        this.challengeService.checkEmailForChallenge(this.state.modalEmail, this.props.challenge.Id).then((res : boolean) => {
            if (res){
                this.setState({
                    showModal : false
                });
                sessionStorage.setItem("NotShowModalForChallenge"+String(this.props.challenge.Id), "1");
            } else {
                this.setState({
                    showModalError : true
                })
            }
        })
    }

}
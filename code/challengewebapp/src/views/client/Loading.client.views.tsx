// src/views/client/Loading.client.views.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Titles from 'src/components/Titles.components';
import loading from '../../assets/img/loading.gif';

export default class Loading extends React.Component<any,any> { 
	
	constructor(props : any){
		super(props);
    }

    public render(){
        return (
            <div className="loading-view">
              <div className = 'loading'>
                <img src={loading} className="loadingLogo" alt="loading-logo"/>
              </div>
              <Titles title = "Loading..."/>;
            </div>
        )
    }

}
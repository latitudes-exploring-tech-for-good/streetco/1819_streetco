// src/views/client/Profile.client.views.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Titles from 'src/components/Titles.components';
import { Form, TextInput, SelectInput } from 'src/components/FormItems.components';
import { ProfileButton } from 'src/components/ProfileButton.components';
import { ChallengeTeam } from 'src/models/challengeTeam.models';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faChartLine, faTimes } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/services/auth.service';
import { ChallengeService } from 'src/services/challenge.service';
import { texts } from 'src/helpers/texts.helpers';
import Loading from './Loading.client.views';
import { User } from 'src/models/user.models';
import { InfoBar } from 'src/components/InfoBar.components';


library.add(faChartLine);
library.add(faTimes);

// The Profile view corresponds to Tab number 4
export default class Profile extends React.Component<any,any> { 
    
    private authService : AuthService;
    private challengeService : ChallengeService;

	constructor(props : any){
        super(props);
        this.state = {
            disabledTeamSelect : true,
            errors : this.props.profileErrors,
            firstName : this.props.user.FirstName,
            infos : this.props.profileInfos,
            isLoading : false,
            isLoadingBeginning : true,
            lastName : this.props.user.LastName,
            teams : [],
            titleSolo : texts.profileTexts.individualTeamOption1,
            userChallenge : null
        }
        this.authService = new AuthService();
        this.challengeService = new ChallengeService();
        this.handleProgressButtonClick = this.handleProgressButtonClick.bind(this);
        this.handleLogoutButtonClick = this.handleLogoutButtonClick.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setUserChallengeAndTeams = this.setUserChallengeAndTeams.bind(this);
        this.closeInfoBar = this.closeInfoBar.bind(this);
        this.isLoading = this.isLoading.bind(this);
        this.closeErrorBar = this.closeErrorBar.bind(this);
    }

    public componentDidMount() {
        this.setUserChallengeAndTeams();
    }

    public componentWillReceiveProps(newProps : {user : User, profileInfos : string[], profileErrors : string[]}){
        this.setState({
            errors : newProps.profileErrors,
            firstName : newProps.user.FirstName,
            infos : newProps.profileInfos,
            lastName : newProps.user.LastName
        })
    }

    public render() {
        if (!this.state.isLoadingBeginning){
            const infoBar = JSON.stringify(this.state.infos) !== JSON.stringify([])? (
                <InfoBar close={this.closeInfoBar} infos={this.state.infos} class="alert alert-success"/>
            ) : "";
            const errorBar = JSON.stringify(this.state.errors) !== JSON.stringify([])? (
                <InfoBar close={this.closeErrorBar} infos={this.state.errors} class="alert alert-danger"/>
            ) : "";
            const teamIdSelected : number = this.state.userChallenge? this.state.userChallenge.TeamChallengeId : null;
            const selectedValue : number = teamIdSelected === null ? 0 : teamIdSelected;
            const indivOption = <option value={0} key={0}>{this.state.titleSolo}</option>
            const teamsList = this.state.teams? this.state.teams.map((team : ChallengeTeam) => 
                <option value={team.Id} key={team.Id}>{team.Name}</option>
            ) : [];
            teamsList.push(indivOption);
            const form = !this.state.isLoading? (
                <Form formClass='update-form' formMethod='POST' validationButton={true} validationButtonText={texts.profileTexts.submitButtonText} buttonName="update" handleSubmit={this.handleSubmit}>
                    <div className="row">
                        <TextInput label={texts.profileTexts.firstNameLabel} type="text" id="firstName" name="firstname" value={this.state.firstName} placeHolder={texts.profileTexts.firstNamePlaceholder} handleChange={this.handleFirstNameChange} className="form-group text-left col-lg-6"/>
                        <TextInput label={texts.profileTexts.lastNameLabel} type="text" id="lastName" name="lastName" value={this.state.lastName} placeHolder={texts.profileTexts.lastNamePlaceholder} handleChange={this.handleLastNameChange} className="form-group text-left col-lg-6"/>
                    </div>
                    <div className="row">
                        <TextInput label={texts.profileTexts.emailLabel} type="email" id="" name="" value={this.props.user.Email} placeHolder={texts.profileTexts.emailPlaceholder} handleChange={this.handleEmailChange} disabled={true} className="form-group text-left col-lg-6"/>
                        <SelectInput id="team" name="team" label={texts.profileTexts.teamLabel} disabled={this.state.disabledTeamSelect} options={teamsList} defaultValue={selectedValue} className="form-group text-left col-lg-6"/>
                    </div>
                </Form>
            ) : <Loading/>;
            return (
                <div className="row bg-w-semi-trans padding-15">
                    <div className="col-lg-12 text-center">
                        <Titles title={texts.profileTexts.title}/>
                        <h2 className="profile-buttons">
                            <div>
                                <ProfileButton buttonClass="btn btn-primary" buttonText={texts.profileTexts.progressButton} icon={faChartLine} handleClick={this.handleProgressButtonClick} disabled={!this.state.userChallenge}/>
                                <ProfileButton buttonClass="btn btn-secondary" buttonText={texts.profileTexts.logoutButton} icon={faTimes} handleClick={this.handleLogoutButtonClick} disabled={false}/>
                            </div>
                        </h2>
                        {infoBar}
                        {errorBar}
                        {form}
                    </div>
                </div>
            );
        } else {
            return <Loading/>
        }
    }

    private handleProgressButtonClick(){
        this.props.handleProgressButtonClick(this.state.userChallenge);
    }

    private handleLogoutButtonClick(){
        this.props.isLoading();
        return this.authService.logout(this.props.tokenObject).then(() => {
            this.props.handleLogoutButtonClick();
            this.props.isLoading();
        })
    }

    private handleFirstNameChange(e : any){
        this.setState({firstName : e.target.value})
    }

    private handleLastNameChange(e : any){
        this.setState({lastName : e.target.value})
    }

    private handleEmailChange(e : any){
        this.setState({email : e.target.value})
    }

    private handleSubmit(e : any){
        e.preventDefault();
        this.isLoading();
        const errors : string[] = [];
        if (this.state.firstName.replace(/ /g, "") === ""){
            errors.push(texts.profileTexts.firstNameError)
        }
        if (this.state.lastName.replace(/ /g, "") === ""){
            errors.push(texts.profileTexts.lastNameError)
        }
        if (errors.length > 0){
            this.setState({
                errors : this.state.errors.concat(errors)
            }, () => {
                this.isLoading();
            })
        }
        else {
            this.authService.updateNames(this.props.tokenObject, this.state.firstName, this.state.lastName).then((res) => {
                if (!res){
                    this.setState({
                        errors : this.state.errors.concat([texts.profileTexts.updateNamesError])
                    }, () => {
                        this.isLoading();
                    })
                } else {
                    this.isLoading();
                    this.props.updateAppState([texts.profileTexts.updateNamesMessage], this.state.errors);
                }
            });
        }        
    }

    private setUserChallengeAndTeams() {
        return this.challengeService.getUserChallengeInfos(this.props.user.Id).then(userChallenge => {
            if (userChallenge){
                this.challengeService.getChallengeTeamsFromChallengeId(userChallenge.ChallengeId).then(teams => {
                    this.setState({
                        isLoadingBeginning : false,
                        teams,
                        userChallenge
                    })
                })
            } else {
                this.setState({
                    isLoadingBeginning : false
                })
            }
        })
    }

    private closeInfoBar(){
        this.setState({
            infos : [],
        });
        this.props.clearProfileInfos();
    }

    private closeErrorBar(){
        this.setState({
            errors : [],
        });
        this.props.clearProfileErrors();
    }

    private isLoading(){
        this.setState({
            isLoading : !this.state.isLoading
        })
    }

}
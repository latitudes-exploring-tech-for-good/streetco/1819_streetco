// src/views/client/Subscription.client.views.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Titles from 'src/components/Titles.components';
import { texts } from 'src/helpers/texts.helpers';
import { Form, TextInput, SelectInput } from 'src/components/FormItems.components';
import { ChallengeService } from 'src/services/challenge.service';
import { Challenge } from 'src/models/challenge.models';
import { ChallengeTeam } from 'src/models/challengeTeam.models';
import { UserChallenge } from 'src/models/userChallenge.models';
import { AuthService } from 'src/services/auth.service';
import { InfoBar } from 'src/components/InfoBar.components';
import { User } from 'src/models/user.models';
import Loading from './Loading.client.views';


// The Subscription view corresponds to Tab number 1
export default class Subscription extends React.Component<any,any> { 
    
    private challengeService : ChallengeService;
    private authService : AuthService;
    
	constructor(props : any){
        super(props);
        this.state={
            challenges : [],
            email : "",
            errors : [],
            errorsGreen : [],
            firstName : "",
            isLoading : false,
            lastName : "",
            nbParticipantsInSelectedTeam : "",
            password : "",
            selectedChallengeId : null,
            selectedTeamId : null,
            teams : [],
            teamsList : [<option value={-1} key={-1} disabled={true}>{texts.subscriptionTexts.teamPlaceholder}</option>] // for the teams options
        }
        this.challengeService = new ChallengeService();
        this.authService = new AuthService();
        this.updateSelectedChallenge = this.updateSelectedChallenge.bind(this);
        this.updateSelectedTeam = this.updateSelectedTeam.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.updateTeams = this.updateTeams.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setTeamsList = this.setTeamsList.bind(this);
        this.closeErrorBar = this.closeErrorBar.bind(this);
        this.closeErrorGreenBar = this.closeErrorGreenBar.bind(this);
    }

    
    public componentDidMount(){
        this.setChallengesList();
    }

    public render() {
        const challengesList = this.state.challenges.map((challenge : Challenge) =>
            <option value={challenge.Id} key={challenge.Id} id={String(challenge.Id)}>{challenge.Name}</option>
        );
        challengesList.push(<option value={0} key={0} disabled={true}>{texts.subscriptionTexts.challengePlaceholder}</option>);
        const errorBar = this.state.errors.length > 0? <InfoBar class="alert alert-danger" infos={this.state.errors} close={this.closeErrorBar}/> : "";
        const errorGreenBar = this.state.errorsGreen.length > 0? <InfoBar class="alert alert-info" infos={this.state.errorsGreen} close={this.closeErrorGreenBar}/> : "";
        const cguLink : string = this.props.language === "fr"? "https://fr.street-co.com/cgu" : "https://en.street-co.com/terms-of-service";
        const form = this.state.isLoading? <Loading/> : (
            <Form formClass='inscription-form' formMethod='POST' validationButton={true} validationButtonText={texts.subscriptionTexts.submitButtonText} handleSubmit={this.handleSubmit}>
                <div className="row">
                    <TextInput label={texts.subscriptionTexts.surnameLabel} type="text" id="surname" name="surname" placeHolder={texts.subscriptionTexts.surnamePlaceholder} value={this.state.lastName} className="form-group text-left col-lg-6" handleChange={this.handleLastNameChange}/>
                    <TextInput label={texts.subscriptionTexts.nameLabel} type="text" id="name" name="name" placeHolder={texts.subscriptionTexts.namePlaceholder} value={this.state.firstName} className="form-group text-left col-lg-6" handleChange={this.handleFirstNameChange}/>
                </div>
                <div className="row">
                    <TextInput label={texts.subscriptionTexts.emailLabel} type="text" id="email" name="email" placeHolder={texts.subscriptionTexts.emailPlaceholder} value={this.state.email} className="form-group text-left col-lg-6" handleChange={this.handleEmailChange}/>
                    <SelectInput id="challenge" name="challenge" defaultValue={0} label={texts.subscriptionTexts.challengeLabel} options={challengesList} value={this.state.selectedChallengeId} handleChange={this.updateSelectedChallenge} className="form-group text-left col-lg-6"/>
                </div>
                <div className="row">
                    <TextInput label={texts.subscriptionTexts.passwordLabel} type="password" id="password" name="password" placeHolder={texts.subscriptionTexts.passwordPlaceholder} value={this.state.password} handleChange={this.handlePasswordChange} className="form-group text-left col-lg-6"/>
                    <SelectInput id="team" name="team" label={texts.subscriptionTexts.teamLabel} defaultValue={-1} label2={texts.subscriptionTexts.teamLabel2 + this.state.nbParticipantsInSelectedTeam} options={this.state.teamsList} value={this.state.selectedTeamId} handleChange={this.updateSelectedTeam}/>
                </div>
                <div className="row">
                    <div className="col-lg-6 offset-lg-3">
                        <input type="checkbox" id='validate_cgu' name='validate_cgu' required={true}/> {texts.subscriptionTexts.checkBox1} <a href={cguLink} target="_blank">{texts.subscriptionTexts.checkBox2} </a>{texts.subscriptionTexts.checkBox3}
                    </div>
                </div>
            </Form>
        )
        return(
            <div className="row bg-w-semi-trans padding-15">
                <div className="col-lg-12 text-center">
                    <Titles title={texts.subscriptionTexts.title} subtitle={texts.subscriptionTexts.subtitle}/>
                    {errorBar}
                    {errorGreenBar}
                    {form}
                </div>
            </div>
        );
    }

    private updateSelectedChallenge(e : any){
        this.setState({
            selectedChallengeId : Number(e.target.value),
            teams : [],
            teamsList : [<option value={-1} key={-1} disabled={true}>{texts.subscriptionTexts.teamPlaceholder}</option>] 
        }, () => {
            this.updateTeams();
        });
         
    }

    private updateTeams(){
        if(this.state.selectedChallengeId){
            this.challengeService.getChallengeTeamsFromChallengeId(this.state.selectedChallengeId).then((teams : ChallengeTeam[]) => {
                this.setState({
                    teams
                }, () => {
                    this.setTeamsList()
                });
            })
        }  
    }

    private setChallengesList() {
        this.challengeService.getChallenges().then(challenges =>{
            this.setState({challenges})
        })
    }

    private handleLastNameChange(e : any){
        this.setState({
            lastName : e.target.value
        })
    }

    private handleFirstNameChange(e : any){
        this.setState({
            firstName : e.target.value
        })
    }

    private handleEmailChange(e : any){
        this.setState({
            email : e.target.value
        })
    }

    private handlePasswordChange(e : any){
        this.setState({
            password : e.target.value
        })
    }

    private updateSelectedTeam(e : any){
        e.persist();
        this.challengeService.getUserChallengesInChallenge(this.state.selectedChallengeId).then((userChallenges : UserChallenge[]) => {
            let count : number = 0;
            userChallenges.forEach((userChallenge : UserChallenge) => {
                if (userChallenge.TeamChallengeId === Number(e.target.value)){
                    count += 1;
                }
            });
            this.setState({
                nbParticipantsInSelectedTeam : count,
                selectedTeamId : Number(e.target.value)
            })
        })
    }

    private handleSubmit(e : any){
        e.preventDefault();
        const errors : string[] = [];
        const errorsGreen : string[] = []
        if (this.state.firstName.replace(/ /g, "") === ""){ // check if firstName and lastName are valid
            errors.push(texts.subscriptionTexts.firstNameError);
        }
        if (this.state.lastName.replace(/ /g, "") === ""){
            errors.push(texts.subscriptionTexts.lastNameError);
        }
        this.setState({
            isLoading : true
        }, () => {
            this.challengeService.checkEmailForRegistration(this.state.email).then((res : any) => { // check if email is an email of an existing streetco account that isn't challenger yet
                if (res ==="err"){ // case of server error
                    errors.push(texts.subscriptionTexts.connexionWithDataBaseError);
                    this.setState({
                        errors,
                        isLoading : false
                    })
                } else if (res) {
                    this.authService.login(this.state.email, this.state.password).then((res1 : any) => {
                        if (!res1){
                            errors.push(texts.subscriptionTexts.loginError);
                            this.setState({
                                errors,
                                isLoading : false
                            })
                        } else {
                            this.challengeService.registerUser(res1.user.Id, this.state.email, String(this.state.selectedChallengeId), String(this.state.selectedTeamId), this.state.firstName, this.state.lastName).then((res2 : any) => {
                                if (res2 === "err"){
                                    errors.push(texts.subscriptionTexts.registerError);
                                    this.setState({
                                        errors,
                                        isLoading : false
                                    });
                                } else {
                                    errorsGreen.push(texts.subscriptionTexts.registerSuccess);
                                    this.authService.getConnectedUser(res1.tokenObject).then((user : User) => {
                                        this.props.handleRegistration(res1.tokenObject, user, errorsGreen, []);
                                    })
                                }
                            })
                        }
                    })
                } else {
                    errors.push(texts.subscriptionTexts.emailError);
                    this.setState({
                        errors,
                        isLoading : false
                    })
                }
            })
        })
    }

    private setTeamsList(){
        const teamsList2 = this.state.teams.map((team : ChallengeTeam) =>
            <option value={team.Id} key={team.Id} id={String(team.Id)}>{team.Name}</option>
        );
        this.challengeService.getChallengeFromChallengeId(this.state.selectedChallengeId).then((challenge : Challenge) => {
            if (challenge.AcceptSolo){
                teamsList2.push(<option value={0} key={0}>{texts.subscriptionTexts.soloTeamName}</option>);
            }
            this.setState({
                teamsList : this.state.teamsList.concat(teamsList2)
            })
        })
    }

    private closeErrorBar(){
        this.setState({
            errors : []
        })
    }

    private closeErrorGreenBar(){
        this.setState({
            errorsGreen : []
        })
    }

}

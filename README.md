# streetco x Latitudes


## Context
* Streetco is a start-up that develops a GPS for people with reduced mobility.
* Streetco is engaged in improving people with reduced mobility's moving conditions.
* More than 90000 obstacles and POI in more than 8600 cities.
* https://street-co.com/

## About the project
* This project aims to create a webapp which will allow organizations such as companies to organize Streetco challenges for their members. This will have benefits on both sides : the participating organizations will increase their team spirit, and Streetco will gain a lot of data for their GPS, helping people with disabilities to move in cities.
* Goals of the project : see the Milestones on the gitlab wiki
* This project comes from a partnership between Streetco and Latitudes, a start-up that has built a community of people who wish to take advantage of their technical skills in order to work on projects with social impact : the Tech for Good Explorers.

## Evolution perspectives
* For this project, we were given two milestones. The milstone 1.0 was to develop all the client side app in french, which we did. The milestone 1.1 was to add an english version of the web application, which we also did, and to integrate the admin side of the current php web site into the webapp. This was a bit too much work for the time we had, so this could be an evolution perspective.
* Another evolution perspective could be to quickly transform the React web application we created into a React Native mobile app. In fact, even though we made the web application as responsive as we could on smaller screens, it will display in the web view of the Streetco mobile app on phones, and it could be nice to have it as a mobile app that could act as an extension of the current Streetco app.

## Contact
* Aurélien Pasteau - Student : 
    Email : aurelien.pasteau@student.ecp.fr
    Phone : +33631285188
* Lorena Torres - Student : 
    Email : lorena.torres-lahoz@student.ecp.fr
    Phone : +34681160195
* Maxence de Bigault Casanove - Student : 
    Email : maxence.de-bigault-casanove@student.ecp.fr
    Phone : +33613620618
* Iheb Belgacem - Student :
    Email : iheb.belgacem@student.ecp.fr
    Phone : +33620019659
* Cyril Bras - Streetco CTO : 
    Email : cyril.bras@street-co.com
    Phone : +33662657153
* Apollinaire Lecocq - Latitudes Mentor : 
    Email : apollinaire.lecocq@live.fr
    Phone : +33664498335               
